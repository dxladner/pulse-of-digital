<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KeyPerformanceIndicator;

class Objective extends Model
{
  protected $fillable = [ 'name', 'label' ];

  public function kpis()
  {
    return $this->belongsToMany(KeyPerformanceIndicator::class, 'kpi_objective', 'objective_id', 'kpi_id');
  }

}
