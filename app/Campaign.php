<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Campaign extends Model
{
    protected $fillable = [ 'name', 'status', 'description', 'start_date', 'end_date', 'budget', 'client_id', 'user_id' ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function roles()
    {
      return $this->belongsToMany(Role::class);
    }

    public function notes()
    {
      return $this->morphMany(Note::class, 'notable');
    }

    public function goals()
    {
        return $this->hasMany(Goal::class);
    }
}