<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
      return $this->belongsToMany(Role::class);
    }

    public function getRoles($user_id)
    {
        if(!empty($user_id))
        {
          $role_id = DB::select('SELECT role_id FROM role_user WHERE user_id = ?', [$user_id]);
          $roleNameArray = [];
          foreach ($role_id as $id)
          {
            $role_name = DB::select('SELECT name FROM roles WHERE id = ?', [$id->role_id]);
            foreach ($role_name as $name)
            {
              $roleNameArray[] = $name;
            }
          }
          return $roleNameArray;
        }
        return false;
    }

    public function hasRole($role)
    {
        if(is_string($role)) {
          return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    public function notes()
    {
      return $this->hasMany(Note::class);
    }
}
