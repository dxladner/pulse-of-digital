<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MediaCategory;
use App\KeyPerformanceIndicator;

class Channel extends Model
{
  protected $fillable = [ 'name', 'label', 'media_category_id', 'media_category_name' ];

  public function mediacategory()
  {
    return $this->belongsToMany(MediaCategory::class);
  }

  public function adFormats() {
    return $this->belongsToMany(Channel::class, 'ad_format_channel_kpi', 'channel_id', 'ad_format_id')->withPivot('kpi_id');
  }

  public function kpis() {
	return $this->belongsToMany(KeyPerformanceIndicator::class, 'ad_format_channel_kpi', 'channel_id', 'kpi_id')->withPivot('ad_format_id');
  }

}
