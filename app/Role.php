<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [ 'name', 'label' ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
    * The users that belong to the role.
    */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function campaigns()
    {
      return $this->belongsToMany(Campaign::class);
    }

    public function hasPermission($permission)
    {
        if(is_string($permission)) {
          return $this->permissions->contains('name', $permission);
        }

        foreach($permission as $p)
        {
          if($this->hasPermission($p->name))
          {
            return true;
          };
        }

        return false;
    }

}
