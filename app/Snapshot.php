<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Snapshot extends Model
{
    protected $fillable = ['name', 'project_id', 'spend', 'week', 'kpis'];

    protected $casts = [
	    'kpis' => 'array', // Will convarted to (Array)
	];

    public function project()
    {
        return $this->hasOne(Project::class);
    }

    public function reports() {
    	return $this->belongsToMany(Report::class);
    }
}
