<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [ 'name', 'fiscal_year_start' ];

    public function campaigns()
    {
        return $this->hasMany('Campaign');
    }

}
