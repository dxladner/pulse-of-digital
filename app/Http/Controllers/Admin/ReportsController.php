<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Project;
use App\Snapshot;
use App\Report;
use App\Campaign;

class ReportsController extends Controller
{

	public function index()
    {
		$snapshots = Snapshot::all();
		$campaigns = Campaign::all();
		$reports = Report::all();

    	return view('admin/reports/index', compact('snapshots', 'campaigns', 'reports'));
    }

    public function storeReport(Request $request)
    {
    	$report = new Report();
    	$report->name = $request->name;
    	$report->save();

    	$snapshots = json_encode($request->snapshot_id);

    	DB::table('report_snapshot')->insert(
		    ['report_id' => $report->id, 'snapshot_id' => $snapshots]
		);

    	alert()->success('Success!', 'Report Created');

    	return redirect()->back();
    }

    public function showReport($id)
    {
    	$report = Report::findOrFail($id);

    	return view('admin/reports/show', compact('report'));

    }

    public function buildReport(Request $request) {

    	$snapshot_ids = DB::table('report_snapshot')
    					->where('report_id', $request->report_id)
    					->value('snapshot_id');

    	$snapshot_ids = json_decode($snapshot_ids);

    	$snapshots = Snapshot::find($snapshot_ids)->toArray();
    	// $kpis = array();

    	// foreach ($snapshots as $snapshot) {
    	// 	array_push($kpis, $snapshot->kpis)
    	// }

    	return response()->json(['snapshots' => $snapshots], 200);
    }

    public function deleteReport($id)
    {
    	$report = Report::findOrFail($id);

    	DB::table('report_snapshot')
    		->where('report_id', $id)
    		->delete();

    	$report->delete();

    	return response()->json(['responseText' => 'Report Deleted!'], 200);
    }

    public function snapshot($id)
    {
		$project = Project::find($id);
		$project_kpis = $project->kpis;

    	return view('admin/reports/snapshot', compact('project', 'project_kpis'));
    }

    public function storeSnapshot(Request $request)
    {
    	$kpis = json_encode($request->kpis);
    	$week = $request->week;

    	if($week == "") {
    		$week = NULL;
    	}

    	Snapshot::create([
    		'name' => $request->name, 
			'project_id' => $request->project_id, 
			'spend' => $request->spend, 
			'kpis' => $kpis, 
			'week' => $week
    	]);

    	return redirect()->back();

    }

    public function campaignData(Request $request)
    {
    	$snapshots = DB::table('projects')
    		->where('campaign_id', $request->campaign_id)
    		->join('snapshots', 'projects.id', '=', 'snapshots.project_id')
    		->select('snapshots.*')
            ->get();

    	return response()->json(['response' => $snapshots], 200);
    }
}








