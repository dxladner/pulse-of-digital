<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjectFormRequest;
use App\Http\Requests;
use App\Project;
use App\Channel;
use App\MediaCategory;
use App\Objective;
use App\AdFormat;
use App\Campaign;
use Auth;
use DB;
use App\User;
use App\Permission;
use App\Role;
use App\Client;
use Carbon\Carbon;
use App\KeyPerformanceIndicator;
use App\Note;

class ProjectsController extends Controller
{
  public function index($id)
  {
    if (Auth::check())
    {
      $campaign = Campaign::findOrFail($id);
      $client = $campaign->client;
      $projects = Project::where('campaign_id', $id)->get();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');
      $channels = Channel::all();
      $media_categories = MediaCategory::all();
      $objectives = Objective::all();
      $campaigns = Campaign::all();
      $ad_formats = AdFormat::all();
      $user_name = DB::table('users')->where('id', $user_id)->value('name');
      $kpis = KeyPerformanceIndicator::all();
      // dd($projects);
      $goals = DB::table('goals')
            ->join('kpis', 'kpis.id', '=', 'goals.kpi_id')
            ->select('goals.*', 'kpis.label')
            ->where('goals.campaign_id', $id)
            ->get();

      return view('admin/projects/index', compact('projects', 'role_name', 'channels', 'media_categories', 'objectives', 'ad_formats', 'campaigns', 'user_id', 'user_name', 'kpis', 'client', 'campaign', 'goals'));
    }
  }

  public function newProject(ProjectFormRequest $request)
  {

      $user_id = DB::table('users')->where('name', $request->get('user_id'))->value('id');
      $today = Carbon::create()->today();
      
      $project = new Project(array(
          'name'   	          => $request->get('project_name'),
          'budget'            => $request->get('project_budget'),
          'labor_cost'        => $request->get('project_labor_cost'),
          'misc_cost'         => $request->get('project_misc_cost'),
          'media_category_id' => $request->get('media_category_id'),
          'channel_id'        => $request->get('channel_id'),
          'objective_id'      => $request->get('objective_id'),
          'ad_format_id'      => $request->get('ad_format_id'),
          'campaign_id'       => $request->get('campaign_id'),
          'user_id'           => $user_id
      ));

      $project_start_date  = Carbon::parse($request->get('project_start_date'));
      $project_end_date    = Carbon::parse($request->get('project_end_date'));
      $project->start_date = $project_start_date->format('Y-m-d');
      $project->end_date   = $project_end_date->format('Y-m-d');

      if($today->between($project_start_date,$project_end_date)) {
        $project->status = 'active';
      } elseif ($project_start_date->isFuture()) {
        $project->status = 'pending';
      }

      $project->save();

      if(count($request->get('kpis')) > 0) {
          $project->kpis()->attach($request->get('kpis'));
      }

      return response()->json(['responseText' => 'Project Created!'], 200);

  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $project = Project::findOrFail($id);
        $user_id = Auth::user()->id;
        $media_category = MediaCategory::findOrFail($project->media_category_id);
          $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
          $role_name = DB::table('roles')->where('id', $role_id)->value('name');
          $channels = Channel::all();
          $media_categories = MediaCategory::all();
          $objectives = Objective::all();
          $campaigns = Campaign::all();
          $ad_formats = AdFormat::all();
          $user_name = DB::table('users')->where('id', $project->user_id)->value('name');
          $kpis = KeyPerformanceIndicator::all()->keyBy('id');
          $selected_kpis = $project->kpis;

          foreach ($selected_kpis as $selected_kpi) {
              if($kpis->contains($selected_kpi)) {
                $kpis->forget($selected_kpi->id);
              }
          }

        return view('admin/projects/edit', compact('project', 'role_name', 'channels', 'media_categories', 'objectives', 'ad_formats', 'campaigns', 'user_id', 'user_name', 'kpis', 'media_category', 'selected_kpis'));
      }
  }

  public function update($id, ProjectFormRequest $request)
  {

      $user_id = DB::table('users')->where('name', $request->get('user_id'))->value('id');

      $project = Project::findOrFail($id);
      $project->name              = $request->get('project_name');
      $project->status            = $request->get('project_status');

      $project_start_date         = $request->get('project_start_date');
      $project_end_date           = $request->get('project_end_date');
      $project->start_date        = Carbon::parse($project_start_date)->format('Y-m-d');
      $project->end_date          = Carbon::parse($project_end_date)->format('Y-m-d');

      $project->budget            = $request->get('project_budget');
      $project->labor_cost        = $request->get('project_labor_cost');
      $project->misc_cost         = $request->get('project_misc_cost');
      $project->channel_id        = $request->get('channel_id');
      $project->media_category_id = $request->get('media_category_id');
      $project->campaign_id       = $request->get('campaign_id');
      $project->ad_format_id      = $request->get('ad_format_id');
      $project->objective_id      = $request->get('objective_id');
      $project->user_id           = $user_id;

      $project->save();

      $campaign_id = $project->campaign_id;

        if(count($request->get('kpis')) > 0) {
            $project->kpis()->detach();
            $project->kpis()->attach($request->get('kpis'));
        }

      return response()->json(['responseText' => 'Project Updated!', 'campaign_id' => $project->campaign_id], 200);
  }

  public function destroy($id)
  {
      $project = Project::findOrFail($id);
      $campaign_id = $project->campaign_id;
      $project->kpis()->detach();
      $project->delete();

        return response()->json(['responseText' => 'Project Deleted!'], 200);

  }

    public function associateKpis(Request $request) {

        $channel_id = $request->channel_id;
        $ad_format_id = $request->ad_format_id;

        $channel = Channel::find($channel_id);
        $grouped_kpis = $channel->kpis()->wherePivot('ad_format_id',$ad_format_id)->get()->unique();

        if($request->has('objective_id')) {
            $objective_id = $request->objective_id;
            $objective = Objective::find($objective_id);
            $objective_kpis = $objective->kpis;
            $all_kpis = $grouped_kpis->merge($objective_kpis);
            return response()->json(['response' => $all_kpis], 200);

        }

        return response()->json(['response' => $grouped_kpis], 200);
    }

    public function storeNote(Request $request)
    {
      $user_id = Auth::user()->id;
      $project = Project::find($request->project_id);
      $note = Note::create(['body' => $request->note_body, 'user_id' => $user_id]);
      $project->notes()->save($note);

      alert()->success('Success!', 'Note Posted');

      return redirect()->back();
    }
}




