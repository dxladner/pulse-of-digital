<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
use App\User;
use App\Role;
use Auth;
use DB;
use Response;
use Input;
use Hash;

class UsersapiController extends Controller
{
    public function index()
    {
      $users = User::all();

      return Response::json([
        //'data'  => $users->toArray()
        'data'  => $this->transformCollection($users)
      ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store() {
      if (Input::has('user_name') and Input::has('user_email') and Input::has('user_password'))
      {
        $user = new User();
        $user->name     = Input::get('user_name');
        $user->email    = Input::get('user_email');
        $user->password = Hash::make(Input::get('user_password'));
        $user->save();

        return Response::json([
          'token'=>csrf_token(),
          'message'	=> 'User successfully created.'

        ], 201);
      }
      else
      {
        return Response::json([

          'error'	=> [
            'message'	=> 'Parameters failed validation for this user'
          ]
        ], 422);

      }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $user = User::find($id);

        if(! $user )
        {
            return Response::json([
              'error' => [
                'message' => 'User does not exist'
              ]

            ], 404);
        }

        return Response::json([
          'data'  => $this->transform($user->toArray())
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id) {
        /* Name Casting
          This comment is for documentation purposes only:

          'user_store'					=> $user['store_id']
          'user_firstname'				=> $user['first_name']
          'user_lastname'					=> $user['last_name']
          'user_email'					=> $user['email']
          'user_password'					=> $user['password']
          'user_advantagecard'			=> $user['advantage_card']
          'user_shopper_id'				=> $user['shopper_id']
          'user_shopper_token'			=> $user['shopper_token']
          'user_shopper_response'			=> $user['shopper_response']
         */

        $user = User::find($id);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $user = User::find($id);


    }

    /**
     * Transform a collection of Users
     *
     * @param  $users
     * @return array
     */
    private function transformCollection($users)
    {
        return array_map([$this, 'transform'], $users->toArray());
    }

    /**
     * Transform a User
     *
     * @param  $user
     * @return array
     */
    private function transform($user)
    {
        return [
            'user_id'     => $user['id'],
            'user_name'   => $user['name'],
            'user_email'  => $user['email']
        ];
    }

}
