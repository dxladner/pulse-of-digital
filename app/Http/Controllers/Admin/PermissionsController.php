<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionFormRequest;
use App\Http\Requests;
use App\Permission;
use Auth;
use DB;

class PermissionsController extends Controller
{
    public function index()
    {
      if (Auth::check())
      {
        $permissions = Permission::all();
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        return view('admin/permissions/index', compact('role_name', 'permissions'));
      }
    }

    public function newPermission(PermissionFormRequest $request)
    {
        $permission = new Permission(array(
            'name'   	      => $request->get('permission_name'),
            'label'   	    => $request->get('permission_label')
        ));
        $permission->name = strtolower($permission->name);
        $permission->name = str_replace(' ', '_', $permission->name);
        $permission->save();
        
        alert()->success('Success!', 'Permission Created');

        return redirect()->back();
    }

    public function edit($id)
    {
        if (Auth::check())
        {
          $permission = Permission::findOrFail($id);
          $user_id = Auth::user()->id;
          // need  to do a JOIN statement with role_user and roles
          $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
          $role_name = DB::table('roles')->where('id', $role_id)->value('name');

          return view('admin/permissions/edit', compact('role_name', 'permission'));
        }
    }

    public function update($id, PermissionFormRequest $request)
    {
        $permission = Permission::findOrFail($id);
        $permission->name = $request->get('permission_name');
        $permission->label = $request->get('permission_label');
        // format permission name before saving
        $permission->name = strtolower($permission->name);
        $permission->name = str_replace(' ', '_', $permission->name);

        $permission->save();

        alert()->success('Success!', 'Permission Updated');

        return redirect('admin/permissions');
    }

    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        return response()->json(['responseText' => 'Permission Deleted!'], 200);
    }
}
