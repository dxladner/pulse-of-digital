<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;
use Auth;
use DB;

use App\Channel;
use App\AdFormat;
use App\KeyPerformanceIndicator;

class KpiGroupsController extends Controller
{
    public function index()
    {

    	if (Auth::check()) {
	      $user_id = Auth::user()->id;
	      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
	      $role_name = DB::table('roles')->where('id', $role_id)->value('name');
	      $kpis = KeyPerformanceIndicator::all();
      	  $channels = Channel::all();
      	  $ad_formats = AdFormat::all();


	      return view('admin/kpigroups/index', compact('role_name', 'user_id', 'role_id', 'kpis', 'channels', 'ad_formats'));
	    }

    }

    public function newGroup(Request $request) {

    	$channel_id = $request->channel_id;
		$channel = Channel::find($channel_id);
		$grouped_kpis = $channel->kpis()->wherePivot('ad_format_id',$request->ad_format_id)->get()->unique();
		$kpis = collect($request->kpis);

		foreach ($grouped_kpis as $grouped_kpi) {

			DB::table('ad_format_channel_kpi')
				->where('ad_format_id' ,'=', $request->ad_format_id)
				->where('channel_id' ,'=', $request->channel_id)
				->where('kpi_id' ,'=', $grouped_kpi->id)
				->delete();

		}

		foreach ($kpis as $kpi) {
				DB::table('ad_format_channel_kpi')->insert(
					['ad_format_id' => $request->ad_format_id, 'channel_id' => $request->channel_id, 'kpi_id' => $kpi]
				);
		}

		return response()->json(['responseText' => 'KPI Group Saved'], 200);

    }

}
