<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\UserFormRequest;
use App\User;
use App\Role;
use Auth;
use DB;
use View;

class UsersController extends Controller
{
  public function index()
  {
    $user = auth()->user();
    $user_id = Auth::user()->id;

    $userRoleNames = $user->getRoles($user_id);

    if ( ( $user->hasRole( 'admin' ) ) || ( $user->hasRole( 'developer' ) ) )
    {
      $users = User::all();
      $roles = Role::all();
      $user_id = Auth::user()->id;

      return view('admin/users/index', compact('users', 'roles'));
    }

    alert()->error('Permissions!', 'Sorry you do not have the correct permissions');

    return view('admin/dashboard');
  }

  public function newUser(UserFormRequest $request)
  {
      $user = new User(array(
          'name'   	      => $request->get('user_name'),
          'email'   	    => $request->get('user_email'),
          'password'      => bcrypt($request->get('user_password'))
      ));

      $user->save();
      alert()->success('Success!', 'User Created');
      return redirect()->back();
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $user = User::findOrFail($id);
        return view('admin/users/edit', compact('user'));
      }
  }

  public function update($id, UserFormRequest $request)
  {
      $user = User::findOrFail($id);
      $user->name = $request->get('user_name');
      $user->email = $request->get('user_email');

      $user->save();

      alert()->success('Success!', 'User Updated');

      return redirect('admin/users');
  }

  public function destroy($id)
  {
      $user = User::findOrFail($id);
      $user->delete();
      return response()->json(['responseText' => 'User Deleted!'], 200);
  }

  public function showUserMatrix()
	{
      $user = auth()->user();
      if ( ( $user->hasRole( 'admin' ) ) || ( $user->hasRole( 'developer' ) ) )
      {
  			   $roles = Role::all();
  			   $users = User::all();
  			   $us = DB::table('role_user')->select('role_id as r_id','user_id as u_id')->get();
  			   $pivot = [];
  			   foreach($us as $u)
          {
  				    $pivot[] = $u->r_id.":".$u->u_id;
  			   }

			     return view('admin.users.matrix', compact('roles','users','pivot') );
		    }

        alert()->success('Error', 'Sorry, you do not have permissions to view this page.');
	 	    return redirect('admin.users');
	}

  public function updateUserMatrix(Request $request)
	{
  			$bits = $request->get('role_user');
  			foreach($bits as $v)
        {
  				$p = explode(":", $v);
  				$data[] = array('role_id' => $p[0], 'user_id' => $p[1]);
  			}

  			DB::transaction(function () use ($data) {
  			DB::table('role_user')->delete();
  			DB::statement('ALTER TABLE role_user AUTO_INCREMENT = 1');
  			DB::table('role_user')->insert($data);
  			});

        alert()->success('Success!', 'User Role Updated!');
        return redirect()->back();
  	}
}
