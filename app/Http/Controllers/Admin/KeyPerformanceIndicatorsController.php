<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\KeyPerformanceIndicatorFormRequest;
use App\Http\Requests;
use App\KeyPerformanceIndicator;
use App\Client;
use Auth;
use DB;
use App\MediaCategory;
use App\Permission;
use App\Role;


class KeyPerformanceIndicatorsController extends Controller
{
  public function index()
  {
    if (Auth::check())
    {
      $kpis = KeyPerformanceIndicator::all();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');

      return view('admin/kpis/index', compact('kpis', 'role_name', 'user_id', 'role_id'));
    }
  }

  public function newKPI(KeyPerformanceIndicatorFormRequest $request)
  {
      $kpi = new KeyPerformanceIndicator(array(
          'name'   	          => $request->get('kpi_name'),
          'label'             => $request->get('kpi_label')
      ));
      $kpi->save();

      alert()->success('Success!', 'KPI Created');

      return redirect()->back();
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $kpi = KeyPerformanceIndicator::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        return view('admin/kpis/edit', compact('role_name', 'kpi', 'user_id', 'role_id'));
      }
  }

  public function update($id, KeyPerformanceIndicatorFormRequest $request)
  {
      $kpi = KeyPerformanceIndicator::findOrFail($id);
      $kpi->name = $request->get('kpi_name');
      $kpi->label = $request->get('kpi_label');
    
      $kpi->save();

      alert()->success('Success!', 'KPI Updated');

      return redirect('admin/kpis');
  }

  public function destroy($id)
  {
      $kpi = KeyPerformanceIndicator::findOrFail($id);
      $kpi->delete();
      return response()->json(['responseText' => 'KPI Deleted!'], 200);
  }
}
