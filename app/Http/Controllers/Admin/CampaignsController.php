<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CampaignFormRequest;
use App\Http\Requests;
use App\Campaign;
use App\Client;
use App\Project;
use App\Channel;
use App\MediaCategory;
use App\Objective;
use App\AdFormat;
use App\KeyPerformanceIndicator;
use Auth;
use DB;
use App\Permission;
use App\Role;
use Carbon\Carbon;
use App\Note;
use App\Goal;

class CampaignsController extends Controller
{
  public function index()
  {
    if (Auth::check())
    {
      $campaigns = Campaign::all();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');
      $clients = Client::all();

      return view('admin/campaigns/index', compact('campaigns', 'role_name', 'clients', 'user_id', 'role_id', 'kpis'));
    }
  }

  public function newCampaign(CampaignFormRequest $request)
  {
    $today = Carbon::create()->today();
    
      $campaign = new Campaign(array(
          'name'   	    => $request->get('campaign_name'),
          'description' => $request->get('campaign_description'),
          'budget'      => $request->get('campaign_budget'),
          'client_id'   => $request->get('client_id'),
          'user_id'     => $request->get('user_id')
      ));
      $campaign_start_date  = Carbon::parse($request->get('campaign_start_date'));
      $campaign_end_date    = Carbon::parse($request->get('campaign_end_date'));
      $campaign->start_date = $campaign_start_date->format('Y-m-d');
      $campaign->end_date   = $campaign_end_date->format('Y-m-d');

      if($today->between($campaign_start_date,$campaign_end_date)) {
        $campaign->status = 'active';
      } elseif ($campaign_start_date->isFuture()) {
        $campaign->status = 'pending';
      }

      $role_id = $request->get('role_id');
      $campaign->save();
      $campaign->roles()->attach($role_id);

      alert()->success('Success!', 'Campaign Created');

      return redirect()->back();
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $campaign = Campaign::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');
        $clients = Client::all();

        $client_id = DB::table('campaigns')->where('id', $id)->value('client_id');
        $client_name = DB::table('clients')->where('id', $client_id)->value('name');
        return view('admin/campaigns/edit', compact('role_name', 'campaign', 'clients', 'user_id', 'role_id', 'client_id', 'client_name'));
      }
  }

  public function update($id, CampaignFormRequest $request)
  {
      $campaign = Campaign::findOrFail($id);
      $campaign->name         = $request->get('campaign_name');
      $campaign->status       = $request->get('campaign_status');
      $campaign->description  = $request->get('campaign_description');
      $campaign_start_date  = $request->get('campaign_start_date');
      $campaign_end_date    = $request->get('campaign_end_date');
      $campaign->start_date = Carbon::parse($campaign_start_date)->format('Y-m-d');
      $campaign->end_date   = Carbon::parse($campaign_end_date)->format('Y-m-d');
      $campaign->budget       = $request->get('campaign_budget');
      $campaign->client_id    = $request->get('client_id');
      $campaign->user_id      = $request->get('user_id');

      $campaign->save();

      alert()->success('Success!', 'Campaign Updated');

      return redirect('admin/campaigns');
  }

  public function destroy($id)
  {
      $projects = Project::where('campaign_id', $id)->get();
      foreach ($projects as $project) {
        $project->kpis()->detach();
        $project->delete();
      }

      $campaign = Campaign::findOrFail($id);
      $campaign->roles()->detach();
      $campaign->delete();

      return response()->json(['responseText' => 'Campaign Deleted!'], 200);
  }

  public function view($id)
  {

      if (Auth::check())
      {
        $campaign = Campaign::findOrFail($id);
        $client = $campaign->client;
        $projects = collect(DB::table('projects')->where('campaign_id', $id)->get());
        $user_id = Auth::user()->id;
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');
        $channels = Channel::all();
        $media_categories = MediaCategory::all();
        $objectives = Objective::all();
        $campaigns = Campaign::all();
        $ad_formats = AdFormat::all();
        $user_name = DB::table('users')->where('id', $user_id)->value('name');
        $kpis = KeyPerformanceIndicator::all();

        return view('admin/campaigns/view', compact('projects', 'role_name', 'channels', 'media_categories', 'objectives', 'ad_formats', 'campaigns', 'user_id', 'user_name', 'kpis', 'campaign', 'client'));
      }    
  }

  public function storeNote(Request $request)
  {
    $user_id = Auth::user()->id;
    $campaign = Campaign::find($request->campaign_id);
    $note = Note::create(['body' => $request->note_body, 'user_id' => $user_id]);
    $campaign->notes()->save($note);

    alert()->success('Success!', 'Note Posted');

    return redirect()->back();
  }

  public function addGoal(Request $request)
  {
    $goal = Goal::create(['kpi_id' => $request->kpi_id, 'campaign_id' => $request->campaign_id, 'value' => $request->value]);

    alert()->success('Success!', 'Goal Added');

    return redirect()->back();
  }

}
