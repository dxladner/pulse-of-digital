<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientFormRequest;
use App\Http\Requests;
use App\Client;
use Auth;
use DB;
use App\Permission;


class ClientsController extends Controller
{
  public function index()
  {
    if (Auth::check())
    {
      $clients = Client::all();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');

      return view('admin/clients/index', compact('clients', 'role_name'));
    }
  }

  public function newClient(ClientFormRequest $request)
  {
      $client = new Client(array(
          'name'   	          => $request->get('client_name'),
          'fiscal_year_start' => $request->get('client_fiscal_year_start')
      ));

      $client->save();

      alert()->success('Success!', 'Client Created');

      return redirect()->back();
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $client = Client::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        return view('admin/clients/edit', compact('role_name', 'client'));
      }
  }

  public function update($id, ClientFormRequest $request)
  {
      $client = Client::findOrFail($id);
      $client->name = $request->get('client_name');
      $client->fiscal_year_start = $request->get('client_fiscal_year_start');

      $client->save();

      alert()->success('Success!', 'Client Updated');

      return redirect('admin/clients');
  }

  public function destroy($id)
  {
      $client = Client::findOrFail($id);
      $client->delete();
      return response()->json(['responseText' => 'Client Deleted!'], 200);
  }

}
