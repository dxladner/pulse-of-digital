<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChannelFormRequest;
use App\Http\Requests;
use App\Channel;
use App\Client;
use Auth;
use DB;
use App\MediaCategory;
use App\Permission;
use App\Role;

class ChannelsController extends Controller
{
  public function index()
  {
    if (Auth::check())
    {
      $channels = Channel::all();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');
      $media_categories = MediaCategory::all();

      return view('admin/channels/index', compact('channels', 'role_name', 'user_id', 'role_id', 'media_categories'));
    }
  }

  public function newChannel(ChannelFormRequest $request)
  {
      $channel = new Channel(array(
          'name'   	          => $request->get('channel_name'),
          'label'             => $request->get('channel_label'),
          'media_category_id' => $request->get('media_category_id')
      ));
      $channel->save();

      alert()->success('Success!', 'Channel Created');
      return redirect()->back();
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $channel = Channel::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');
        $media_categories = MediaCategory::all();

        $media_category_id = DB::table('channels')->where('id', $id)->value('media_category_id');
        $media_category_name = DB::table('media_categories')->where('id', $media_category_id)->value('name');
        return view('admin/channels/edit', compact('role_name', 'channel', 'media_categories', 'user_id', 'role_id', 'media_category_id', 'media_category_name'));
      }
  }

  public function update($id, ChannelFormRequest $request)
  {
      $channel = Channel::findOrFail($id);
      $channel->name = $request->get('channel_name');
      $channel->label = $request->get('channel_label');
      $channel->media_category_id = $request->get('media_category_id');

      $channel->save();
      alert()->success('Success!', 'Channel Updated');
      return redirect('admin/channels');
  }

  public function destroy($id)
  {
      $channel = Channel::findOrFail($id);
      $channel->delete();
      return response()->json(['responseText' => 'Channel Deleted!'], 200);
  }
}
