<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use DB;
use App\Role;
use App\User;

class DashboardController extends Controller
{
    public function index()
    {
      //$roles = Role::all();
      if (Auth::check())
      {
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        return view('admin/dashboard', compact('role_name'));
      }
    }
}
