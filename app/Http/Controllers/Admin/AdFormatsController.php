<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdFormatFormRequest;
use App\Http\Requests;
use App\AdFormat;
use App\Client;
use Auth;
use DB;
use App\Permission;
use App\Role;


class AdFormatsController extends Controller
{
  public function index()
  {
    if (Auth::check())
    {
      $ad_formats = AdFormat::all();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');

      return view('admin/adformats/index', compact('ad_formats', 'role_name'));
    }
  }

  public function newAdFormat(AdFormatFormRequest $request)
  {
      $ad_format = new AdFormat(array(
          'name'   	   => $request->get('ad_format_name'),
          'label'      => $request->get('ad_format_label')
      ));

      $ad_format->save();

      alert()->success('Success!', 'Ad Format Created');

      return redirect()->back();
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $ad_format = AdFormat::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        return view('admin/adformats/edit', compact('role_name', 'ad_format', 'user_id', 'role_id'));
      }
  }

  public function update($id, AdFormatFormRequest $request)
  {
      $ad_format = AdFormat::findOrFail($id);
      $ad_format->name        = $request->get('ad_format_name');
      $ad_format->label       = $request->get('ad_format_label');

      $ad_format->save();

      alert()->success('Success!', 'Ad Format Updated');

      return redirect('admin/adformats');
  }

  public function destroy($id)
  {
      $ad_format = AdFormat::findOrFail($id);
      $ad_format->delete();
      return response()->json(['responseText' => 'Ad Format Deleted!'], 200);
  }
}
