<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ObjectiveFormRequest;
use App\Http\Requests;
use App\Objective;
use App\Client;
use Auth;
use DB;
use App\Permission;
use App\Role;
use App\KeyPerformanceIndicator;


class ObjectivesController extends Controller
{
  public function index()
  {
    if (Auth::check())
    {
      $objectives = Objective::all();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');
      $kpis = KeyPerformanceIndicator::all();


      return view('admin/objectives/index', compact('objectives', 'role_name', 'kpis'));
    }
  }

  public function newObjective(ObjectiveFormRequest $request)
  {
      $objective = new Objective(array(
          'name'   	    => $request->get('objective_name'),
          'label'      => $request->get('objective_label')
      ));

      $objective->save();

      if(count($request->get('kpis')) > 0) {
          $objective->kpis()->attach($request->get('kpis'));
      }

      return response()->json(['responseText' => 'Objective Created!'], 200);
      
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $objective = Objective::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        $kpis = KeyPerformanceIndicator::all();
        $kpis = $kpis->keyBy('id');
        $current_kpis = $objective->kpis;

        $selected_kpis = array();

        foreach($current_kpis as $current_kpi) {
          if($kpis->contains($current_kpi)) {
              $kpis->forget($current_kpi->id);
              array_push($selected_kpis, $current_kpi);
          }
        }

        return view('admin/objectives/edit', compact('role_name', 'objective', 'user_id', 'role_id', 'kpis', 'selected_kpis'));
      }
  }

  public function update($id, ObjectiveFormRequest $request)
  {
      $objective = Objective::findOrFail($id);
      $objective->name        = $request->get('objective_name');
      $objective->label       = $request->get('objective_label');

      $objective->save();

      $objective->kpis()->detach();

      if(count($request->get('kpis')) > 0) {
          $objective->kpis()->attach($request->get('kpis'));
      }


      return response()->json(['responseText' => 'Objective Updated!'], 200);
  }

  public function destroy($id)
  {
      $objective = Objective::findOrFail($id);
      $objective->kpis()->detach();
      $objective->delete();
      return response()->json(['responseText' => 'Objective Deleted!'], 200);
  }
}
