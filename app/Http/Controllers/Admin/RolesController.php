<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RolesFormRequest;
use App\Http\Requests;
use App\Role;
use Auth;
use DB;
use App\Permission;
use App\User;

class RolesController extends Controller
{
    public function index()
    {
      if (Auth::check())
      {
        $roles = Role::all();
        $user_id = Auth::user()->id;
        $permissions = Permission::all();

        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        return view('admin/roles/index', compact('role_name', 'roles', 'permissions'));
      }
    }

    public function newRole(RolesFormRequest $request)
    {
        $role = new Role(array(
            'name'   	      => $request->get('role_name'),
            'label'   	    => $request->get('role_label')
        ));
        $role->name = strtolower($role->name);
        $role->name = str_replace(' ', '_', $role->name);
        $role->save();

        alert()->success('Success!', 'Role Created');

        return redirect()->back();
    }

    public function addPermission($id)
    {
      if (Auth::check())
      {
        $role = Role::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        //$role_id_permissions = DB::table('permission_role')->where('role_id', $id)->value('role_id');
        // $role_id_permissions = DB::table('permission_role')
        //              ->select(DB::raw('permission_id, '))
        //              ->where('role_id', '=', $id)
        //              ->get();
         $permissions = DB::table('permission_role')
           ->where('role_id', '=', $id)
           ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
           ->join('roles', 'permission_role.role_id', '=', 'roles.id')
           ->select('permission_role.*', 'permissions.id', 'permissions.name', 'permissions.label', 'roles.name')
           ->get();
        //dd($permissions);
        return view('admin/roles/add', compact('role_name', 'permissions'));
      }
    }

    public function edit($id)
    {
        if (Auth::check())
        {
          $role = Role::findOrFail($id);
          $user_id = Auth::user()->id;
          // need  to do a JOIN statement with role_user and roles
          $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
          $role_name = DB::table('roles')->where('id', $role_id)->value('name');

          return view('admin/roles/edit', compact('role_name', 'role'));
        }
    }

    public function update($id, RolesFormRequest $request)
    {
        $role = Role::findOrFail($id);
        $role->name = $request->get('role_name');
        $role->label = $request->get('role_label');
        // format permission name before saving
        $role->name = strtolower($role->name);
        $role->name = str_replace(' ', '_', $role->name);

        $role->save();

        alert()->success('Success!', 'Role Updated');

        return redirect('admin/roles');
    }

    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->delete();
        return response()->json(['responseText' => 'Role Deleted!'], 200);
    }

  /**
	 * A full matrix of roles and permissions.
	 * @return Response
	 */
	public function showRoleMatrix()
	{
    if (Auth::check())
    {
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');

			$roles = Role::all();
			$perms = Permission::all();
			$prs = DB::table('permission_role')->select('role_id as r_id','permission_id as p_id')->get();

			$pivot = [];
			foreach($prs as $p)
      {
				$pivot[] = $p->r_id.":".$p->p_id;
			}
      return view('admin.roles.matrix', compact('roles','perms','pivot', 'role_name') );
		}

	 	return redirect('admin.roles')->with('status', 'U do not have permission!');
	}

  /**
	 * Sync roles and permissions.
	 * @return Response
	 */
	public function updateRoleMatrix(Request $request)
	{
			$bits = $request->get('perm_role');
			foreach($bits as $v)
      {
				$p = explode(":", $v);
				$data[] = array('role_id' => $p[0], 'permission_id' => $p[1]);
			}

        DB::transaction(function () use ($data) {
				DB::table('permission_role')->delete();
				DB::statement('ALTER TABLE permission_role AUTO_INCREMENT = 1');
				DB::table('permission_role')->insert($data);
			});

      alert()->success('Success!', 'Roles Updated');

      return redirect()->back();
	}

}
