<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\MediaCategoryFormRequest;
use App\Http\Requests;
use App\MediaCategory;
use App\Client;
use Auth;
use DB;
use App\Permission;
use App\Role;

class MediaCategoriesController extends Controller
{
  public function index()
  {
    if (Auth::check())
    {
      $media_categories = MediaCategory::all();
      $user_id = Auth::user()->id;
      // need  to do a JOIN statement with role_user and roles
      $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
      $role_name = DB::table('roles')->where('id', $role_id)->value('name');

      return view('admin/mediacategories/index', compact('media_categories', 'role_name'));
    }
  }

  public function newMediaCategory(MediaCategoryFormRequest $request)
  {
      $media_category = new MediaCategory(array(
          'name'   	   => $request->get('media_category_name'),
          'label'      => $request->get('media_category_label')
      ));

      $media_category->save();
      alert()->success('Success!', 'Media Category Created');

      return redirect()->back();
  }

  public function edit($id)
  {
      if (Auth::check())
      {
        $media_category = MediaCategory::findOrFail($id);
        $user_id = Auth::user()->id;
        // need  to do a JOIN statement with role_user and roles
        $role_id = DB::table('role_user')->where('user_id', $user_id)->value('role_id');
        $role_name = DB::table('roles')->where('id', $role_id)->value('name');

        return view('admin/mediacategories/edit', compact('role_name', 'media_category', 'user_id', 'role_id'));
      }
  }

  public function update($id, MediaCategoryFormRequest $request)
  {
      $media_category = MediaCategory::findOrFail($id);
      $media_category->name        = $request->get('media_category_name');
      $media_category->label       = $request->get('media_category_label');

      $media_category->save();

      alert()->success('Success!', 'Media Category Updated');

      return redirect('admin/mediacategories');
  }

  public function destroy($id)
  {
      $media_category = MediaCategory::findOrFail($id);
      $media_category->delete();
      return response()->json(['responseText' => 'Media Category Deleted!'], 200);
  }
}
