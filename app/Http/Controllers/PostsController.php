<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Post;
use App\User;
use Gate;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{
    public function show($id)
    {
      // if (Auth::check())
      // {
      //   $user = Auth::user()->id;
      // }
    //  auth()->loginUsingId(1);

      $post = Post::findOrFail($id);


      // if(Gate::denies('show-post', $post)) {
      //   abort(403, 'Sorry, you cannot see something you did not create');
      // }
      return view('posts.show', compact('post'));
      }

}
