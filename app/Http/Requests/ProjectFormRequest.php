<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'project_name'        => 'required|min:1',
          'project_start_date'  => 'required|min:1',
          'project_end_date'    => 'required|min:1',
          'project_budget'      => 'required|min:1',
          'project_labor_cost'  => 'required|min:1',
          'project_misc_cost'   => 'required|min:1',
          'channel_id'          => 'required|min:1',
          'media_category_id'   => 'required|min:1',
          'campaign_id'         => 'required|min:1',
          'ad_format_id'        => 'required|min:1',
          'objective_id'        => 'required|min:1',
          'user_id'             => 'required|min:1',
      ];
    }
}
