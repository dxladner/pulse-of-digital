<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CampaignFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'campaign_name'         => 'required|min:1',
          'campaign_description'  => 'required|min:1',
          'campaign_start_date'   => 'required|min:1',
          'campaign_end_date'     => 'required|min:1',
          'campaign_budget'       => 'required|min:1',
          'client_id'             => 'required|min:1',
          'user_id'               => 'required|min:1',
          'role_id'               => 'required|min:1',
      ];
    }
}
