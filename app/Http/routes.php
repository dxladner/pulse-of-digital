<?php

/*
|--------------------------------------------------------------------------
| Application Routes for CRUD using JS Frameworks
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(array('prefix'	=> 'api/'), function()
{
	Route::resource('stories', 'StoriesController', ['except' => [ 'create', 'edit' ]]);

	Route::resource('users', 'Admin\UsersapiController');

});

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::resource('posts', 'PostsController');

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/


Route::group( ['middleware' => 'auth' ], function()
{

  Route::get('/admin', 'Admin\DashboardController@index');


  /*----------- Permissions Routes ------------------*/
  Route::get('/admin/permissions', 'Admin\PermissionsController@index');
  Route::post('/admin/permissions', 'Admin\PermissionsController@newPermission');
  Route::get('/admin/permissions/{id?}/edit', 'Admin\PermissionsController@edit');
  Route::post('/admin/permissions/{id?}/edit', 'Admin\PermissionsController@update');
  Route::post('/admin/permissions/{id?}/delete', 'Admin\PermissionsController@destroy');

  /*----------- Users Routes ------------------*/
  Route::get('/admin/users', 'Admin\UsersController@index');
  Route::post('/admin/users', 'Admin\UsersController@newUser');
  Route::get('/admin/users/{id?}/edit', 'Admin\UsersController@edit');
  Route::post('/admin/users/{id?}/edit', 'Admin\UsersController@update');
  Route::post('/admin/users/{id?}/delete', 'Admin\UsersController@destroy');
	Route::get('/admin/users/matrix', 'Admin\UsersController@showUserMatrix');
	Route::post('/admin/users/matrix', 'Admin\UsersController@updateUserMatrix');

  /*----------- Roles Routes ------------------*/
  Route::get('/admin/roles', 'Admin\RolesController@index');
  Route::post('/admin/roles', 'Admin\RolesController@newRole');
  Route::get('/admin/roles/{id?}/edit', 'Admin\RolesController@edit');
  Route::post('/admin/roles/{id?}/edit', 'Admin\RolesController@update');
  Route::post('/admin/roles/{id?}/delete', 'Admin\RolesController@destroy');

  Route::get('/admin/roles/{id?}/add', 'Admin\RolesController@addPermission');
	Route::get('/admin/roles/matrix', 'Admin\RolesController@showRoleMatrix');
	Route::post('/admin/roles/matrix', 'Admin\RolesController@updateRoleMatrix');

	/*----------- Client Routes ------------------*/
  Route::get('/admin/clients', 'Admin\ClientsController@index');
  Route::post('/admin/clients', 'Admin\ClientsController@newClient');
  Route::get('/admin/clients/{id?}/edit', 'Admin\ClientsController@edit');
  Route::post('/admin/clients/{id?}/edit', 'Admin\ClientsController@update');
  Route::post('/admin/clients/{id?}/delete', 'Admin\ClientsController@destroy');

	/*----------- Campaigns Routes ------------------*/
	Route::get('/admin/campaigns', 'Admin\CampaignsController@index');
	Route::post('/admin/campaigns', 'Admin\CampaignsController@newCampaign');
	Route::get('/admin/campaigns/{id?}/edit', 'Admin\CampaignsController@edit');
	Route::post('/admin/campaigns/{id?}/edit', 'Admin\CampaignsController@update');
	Route::post('/admin/campaigns/{id?}/delete', 'Admin\CampaignsController@destroy');
	Route::post('/admin/campaigns/{id?}/notes', 'Admin\CampaignsController@storeNote');
	Route::post('/admin/campaigns/{id?}/goals', 'Admin\CampaignsController@addGoal');

	/*----------- Objectives Routes ------------------*/
	Route::get('/admin/objectives', 'Admin\ObjectivesController@index');
	Route::post('/admin/objectives', 'Admin\ObjectivesController@newObjective');
	Route::get('/admin/objectives/{id?}/edit', 'Admin\ObjectivesController@edit');
	Route::post('/admin/objectives/{id?}/edit', 'Admin\ObjectivesController@update');
	Route::post('/admin/objectives/{id?}/delete', 'Admin\ObjectivesController@destroy');

	/*----------- Media Categories Routes ------------------*/
	Route::get('/admin/mediacategories', 'Admin\MediaCategoriesController@index');
	Route::post('/admin/mediacategories', 'Admin\MediaCategoriesController@newMediaCategory');
	Route::get('/admin/mediacategories/{id?}/edit', 'Admin\MediaCategoriesController@edit');
	Route::post('/admin/mediacategories/{id?}/edit', 'Admin\MediaCategoriesController@update');
	Route::post('/admin/mediacategories/{id?}/delete', 'Admin\MediaCategoriesController@destroy');

	/*----------- Ad Formats Routes ------------------*/
	Route::get('/admin/adformats', 'Admin\AdFormatsController@index');
	Route::post('/admin/adformats', 'Admin\AdFormatsController@newAdFormat');
	Route::get('/admin/adformats/{id?}/edit', 'Admin\AdFormatsController@edit');
	Route::post('/admin/adformats/{id?}/edit', 'Admin\AdFormatsController@update');
	Route::post('/admin/adformats/{id?}/delete', 'Admin\AdFormatsController@destroy');

	/*----------- KPIs Routes ------------------*/
	Route::get('/admin/kpis', 'Admin\KeyPerformanceIndicatorsController@index');
	Route::post('/admin/kpis', 'Admin\KeyPerformanceIndicatorsController@newKPI');
	Route::get('/admin/kpis/{id?}/edit', 'Admin\KeyPerformanceIndicatorsController@edit');
	Route::post('/admin/kpis/{id?}/edit', 'Admin\KeyPerformanceIndicatorsController@update');
	Route::post('/admin/kpis/{id?}/delete', 'Admin\KeyPerformanceIndicatorsController@destroy');

	/*----------- Channel Routes ------------------*/
	Route::get('/admin/channels', 'Admin\ChannelsController@index');
	Route::post('/admin/channels', 'Admin\ChannelsController@newChannel');
	Route::get('/admin/channels/{id?}/edit', 'Admin\ChannelsController@edit');
	Route::post('/admin/channels/{id?}/edit', 'Admin\ChannelsController@update');
	Route::post('/admin/channels/{id?}/delete', 'Admin\ChannelsController@destroy');

	/*----------- Projects Routes ------------------*/
	Route::get('/admin/projects', 'Admin\ProjectsController@index');
	Route::get('/admin/campaigns/{id}', 'Admin\ProjectsController@index');
	Route::post('/admin/campaigns/{campaign_id}', 'Admin\ProjectsController@newProject');
	Route::get('/admin/projects/{id?}/edit', 'Admin\ProjectsController@edit');
	Route::post('/admin/projects/{id?}/edit', 'Admin\ProjectsController@update');
	Route::post('/admin/projects/{id?}/delete', 'Admin\ProjectsController@destroy');
	Route::get('/admin/projects/associatekpis', 'Admin\ProjectsController@associateKpis');
	Route::post('/admin/projects/{id?}/notes', 'Admin\ProjectsController@storeNote');


	/*----------- KPI Groups Routes ------------------*/
	Route::get('/admin/kpigroups', 'Admin\KpiGroupsController@index');
	Route::post('/admin/kpigroups', 'Admin\KpiGroupsController@newGroup');

	/*----------- Reports Routes ------------------*/
	Route::get('/admin/reports', 'Admin\ReportsController@index');
	Route::post('/admin/reports', 'Admin\ReportsController@storeReport');
	Route::get('/admin/reports/{id}', 'Admin\ReportsController@showReport');
	Route::get('/admin/reports/{id}/build', 'Admin\ReportsController@buildReport');
	Route::get('/admin/reports/snapshot/{id?}', 'Admin\ReportsController@snapshot');
	Route::post('/admin/reports/snapshot/{id?}', 'Admin\ReportsController@storeSnapshot');
	Route::get('/admin/reports/campaign/{id}', 'Admin\ReportsController@campaignData');
	Route::post('/admin/reports/{id?}/delete', 'Admin\ReportsController@deleteReport');


});
