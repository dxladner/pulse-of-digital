<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Channel;
use App\KeyPerformanceIndicator;

class AdFormat extends Model
{
    protected $fillable = [ 'name', 'label' ];

    public function channels() {
        return $this->belongsToMany(Channel::class, 'ad_format_channel_kpi', 'ad_format_id', 'channel_id')->withPivot('kpi_id');
    }

    public function kpis() {
        return $this->belongsToMany(Channel::class, 'ad_format_channel_kpi', 'ad_format_id', 'kpi_id')->withPivot('channel_id');
    }
}
