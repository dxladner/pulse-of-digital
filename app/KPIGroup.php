<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\AdFormat;
use App\Channel;
use App\KeyPerformanceIndicator;

class KPIGroup extends Model
{

	protected $table = 'ad_format_channel_kpi';


    public function adFormats() {
    	return $this->hasOne(AdFormat::class);
    }

    public function channels() {
    	return $this->hasOne(Channel::class);
    }

    public function kpis() {
    	return $this->hasMany(KeyPerformanceIndicator::class);
    }
    
}
