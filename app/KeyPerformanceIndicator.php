<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Objective;
use App\AdFormat;
use App\Channel;

class KeyPerformanceIndicator extends Model
{
    protected $table = 'kpis';
    protected $fillable = [ 'name', 'label' ];

    public function objectives()
    {
      return $this->belongsToMany(Objective::class, 'kpi_objective', 'kpi_id', 'objective_id');
    }

    public function projects()
    {
      return $this->belongsToMany(Project::class, 'kpi_project', 'kpi_id', 'project_id');
    }

    public function ad_formats() {
        return $this->belongsToMany(AdFormat::class, 'ad_format_channel_kpi', 'kpi_id', 'ad_format_id')->withPivot('channel_id');
    }

    public function channels() {
        return $this->belongsToMany(Channel::class, 'ad_format_channel_kpi', 'kpi_id', 'channel_id')->withPivot('ad_format_id');
    }
}
