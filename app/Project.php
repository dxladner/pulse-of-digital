<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\KeyPerformanceIndicator;

class Project extends Model
{
    protected $fillable = [ 'name', 'status', 'start_date', 'end_date', 'budget', 'labor_cost', 'misc_cost', 'channel_id', 'media_category_id', 'campaign_id', 'ad_format_id', 'objective_id', 'user_id'  ];

    public function kpis()
    {
      return $this->belongsToMany(KeyPerformanceIndicator::class, 'kpi_project', 'project_id', 'kpi_id');
    }

    public function notes()
    {
      return $this->morphMany(Note::class, 'notable');
    }

    public function channel()
    {
    	return $this->belongsTo(Channel::class);
    }

    public function objective()
    {
    	return $this->belongsTo(Objective::class);
    }

    public function ad_format()
    {
    	return $this->belongsTo(AdFormat::class);
    }
}
