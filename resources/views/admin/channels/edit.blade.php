@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit a Channel</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="">
                <!-- error messages -->
                  @foreach ($errors->all() as $error)
                      <p class="alert alert-danger">{{ $error }}</p>
                  @endforeach
                  <!-- status messages -->
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="channel_name">Channel Name</label>
                    <input type="text" class="form-control" id="channel_name" name="channel_name" value="{!! $channel->name !!}">
                  </div>
                  <div class="form-group">
                    <label for="channel_label">Channel Label</label>
                    <input type="text" class="form-control" id="channel_label" name="channel_label" value="{!! $channel->label !!}">
                  </div>
                  <div class="form-group">
                    <label for="media_category_id">Media Category</label>
                    <select class="form-control" id="media_category_id" name="media_category_id">
                      <option value="{!! $media_category_id !!}">{!! $media_category_name !!}</option>
                      @foreach($media_categories as $media_category)
                          <option value="{!! $media_category->id !!}">{!! $media_category->name !!}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->
        </div><!-- end row -->
      </section>
    </div>
    <!-- /.content-wrapper -->


@endsection
