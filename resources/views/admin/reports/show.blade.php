@extends('admin.layouts.master')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content"><!-- section content -->
		<div class="row">
			<h1 class="text-center">{{ $report->name }}</h1>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<table width="100%">
					<tr>
						<td valign="top" width="50%">
							<div id="myGrid" style="width:600px;height:500px;"></div>
						</td>
					</tr>
				</table>
			</div>
		</div>

	</section>
</div>
<input type="hidden" value="{{ $report->id }}" id="report_id"></input>
@endsection