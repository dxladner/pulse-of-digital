@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->

        <div class="row">
          <h1 class="text-center">Reports</h1>
        </div>

        <div class="row project_btn_row">
          <div class="col-md-1 col-md-offset-1">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
              New Report
            </button>
          </div>
        </div>

        <div class="row"><!-- start row -->

          <!-- right column -->
          <div class="col-md-10 col-md-offset-1">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Reports Table</h3>
                @if ($reports->isEmpty())
                    <p> There are no reports.</p>
                @else
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped">
                  <tbody>

                    <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Delete</th>
                  </tr>
                  @foreach($reports as $report)
                      <tr class="report" id="{{ $report->id }}">
                          <td>{!! $report->id !!}</td>
                          <td>{!! $report->name !!}</td>
                          <td>
                            <form class="delete" method="post" action="<?php echo LARAVEL_URL; ?>/admin/reports/{!! $report->id !!}/delete" >
                              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <div class="form-group">
                                    <div>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                </div>
                            </form>
                          </td>
                      </tr>
                  @endforeach

                </tbody>
              </table>
              @endif
              </div>
              <!-- /.box-body -->
            </div>

          </div><!--/.col (right) -->
        </div><!-- end row -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create New Report</h4>
              </div>
              <div class="modal-body">
                
               <form role="form" method="POST" action="">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name"></input>
                  </div>
                  <div class="form-group">
                    <label for="report_campaign_id">Campaign</label>
                    <div class="campaigns_wrapper">
                      <select class="form-control" id="report_campaign_id" name="campaign_id">
                            <option value="" selected></option>
                        @foreach($campaigns as $campaign)
                            <option value="{!! $campaign->id !!}">{!! $campaign->name !!}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="snapshot_wrap"></div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>

            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
