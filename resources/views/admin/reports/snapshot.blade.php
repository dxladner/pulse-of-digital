@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Report {{ $project->name }}</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="name_field">Name</label>
                    <input type="text" class="form-control" id="name_field" name="name" value="{{ $project->name }} | ">
                  </div>
                  <div class="form-group">
                    <label for="week_field">Week</label>
                    <input type="text" class="form-control" id="week_field" name="week">
                  </div>
                  <div class="form-group">
                    <label for="spend_field">Spend</label>
                    <input type="text" class="form-control" id="spend_field" name="spend">
                  </div>
                  @foreach($project_kpis as $index => $project_kpi)
                  <div class="form-group">
                    <label for="{{ $project_kpi->name }}">{{ $project_kpi->label }} Results</label>
                    <input type="text" class="form-control" id="{{ $project_kpi->name }}" name="kpis[{{$index}}][{{$project_kpi->name}}]">
                  </div>
                  @endforeach
                  <input type="hidden" value="{{ $project->id }}" name="project_id"></input>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->    
        </div><!-- end row -->

      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
