@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Create a Client</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="">
                <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="client_name">Client Name</label>
                    <input type="text" class="form-control" id="client_name" name="client_name">
                  </div>
                  <div class="form-group">
                    <label for="client_fiscal_year_start">Client Fiscal Year Start</label>
                    <input type="date" class="form-control" id="client_fiscal_year_start" name="client_fiscal_year_start">
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->

          <!-- right column -->
          <div class="col-md-6">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Clients Table</h3>
                @if ($clients->isEmpty())
                    <p> There are no clients.</p>
                @else
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped">
                  <tbody>

                    <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Fiscal Year Start</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                  @foreach($clients as $client)
                      <tr>
                          <td>{!! $client->id !!}</td>
                          <td>{!! $client->name !!}</td>
                          <td>{!! $client->fiscal_year_start !!}</td>
                          <td>
                            <a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/admin/clients/{!! $client->id !!}/edit">Edit</a>
                          </td>
                          <td>
                          <form class="delete" method="post" action="<?php echo LARAVEL_URL; ?>/admin/clients/{!! $client->id !!}/delete" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                              <div class="form-group">
                                  <div>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                              </div>
                          </form>
                          </td>
                      </tr>
                  @endforeach

                </tbody>
              </table>
              @endif
              </div>
              <!-- /.box-body -->
            </div>

          </div><!--/.col (right) -->
        </div><!-- end row -->

      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
