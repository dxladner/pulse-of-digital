@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Create User</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="">
                <!-- error messages -->
                  @foreach ($errors->all() as $error)
                      <p class="alert alert-danger">{{ $error }}</p>
                  @endforeach
                  <!-- status messages -->
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="user_name">User Name</label>
                    <input type="text" class="form-control" id="user_name" name="user_name">
                  </div>
                  <div class="form-group">
                    <label for="user_email">User Email</label>
                    <input type="email" class="form-control" id="user_email" name="user_email">
                  </div>
                  <div class="form-group">
                    <label for="user_password">User Password</label>
                    <input type="password" class="form-control" id="user_password" name="user_password">
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->

          <!-- right column -->
          <div class="col-md-6">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Users Table</h3>
                @if ($users->isEmpty())
                    <p> There are no users.</p>
                @else
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped">
                  <tbody>
                    <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                  @foreach($users as $user)
                      <tr>
                          <td>{!! $user->id !!}</td>
                          <td>{!! $user->name !!} </td>
                          <td>{!! $user->email !!}</td>
                          <td>
                          @if(Auth::user()->can('edit_user'))
                            <a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/admin/users/{!! $user->id !!}/edit">Edit</a>
                          @endif
                          </td>
                          <td>
                          @if(Auth::user()->can('delete_user'))
                          <form class="delete" method="post" action="<?php echo LARAVEL_URL; ?>/admin/users/{!! $user->id !!}/delete" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                              <div class="form-group">
                                  <div>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                              </div>
                          </form>
                          @endif
                          </td>
                      </tr>
                  @endforeach
                </tbody>
              </table>
              @endif
              </div>
              <!-- /.box-body -->
            </div>
          </div><!--/.col (right) -->
        </div><!-- end row -->
      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
