@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit a Project</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="" id="edit_project">
                <!-- error messages -->
                  @foreach ($errors->all() as $error)
                      <p class="alert alert-danger">{{ $error }}</p>
                  @endforeach
                  <!-- status messages -->
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="user_id">Project Creator</label>
                    <input class="form-control" readonly type="text" id="user_id" name="user_id" value="{!! $user_name !!}" />
                  </div>
                  <div class="form-group">
                    <label for="project_name">Project Name</label>
                    <input type="text" class="form-control" id="project_name" name="project_name" value="{{$project->name}}">
                  </div>
                  <div class="form-group">
                    <label for="project_status">Project Status</label>
                    <select class="form-control" id="project_status" name="project_status" value="{{$project->status}}">
                      <option value="active">Active</option>
                      <option value="suspend">Suspend</option>
                      <option value="archive">Archive</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="project_start_date">Project Start Date</label>
                    <input type="text" class="form-control" id="project_start_date" name="project_start_date" value="{{$project->start_date}}">
                  </div>
                  <div class="form-group">
                    <label for="project_end_date">Project End Date</label>
                    <input type="text" class="form-control" id="project_end_date" name="project_end_date" value="{{$project->end_date}}">
                  </div>
                  <div class="form-group">
                    <label for="project_budget">Project Budget</label>
                    <input type="text" class="form-control" id="project_budget" name="project_budget" value="{{$project->budget}}">
                  </div>
                  <div class="form-group">
                    <label for="project_labor_cost">Project Labor Cost</label>
                    <input type="text" class="form-control" id="project_labor_cost" name="project_labor_cost" value="{{$project->labor_cost}}">
                  </div>
                  <div class="form-group">
                    <label for="project_misc_cost">Project Misc Cost</label>
                    <input type="text" class="form-control" id="project_misc_cost" name="project_misc_cost" value="{{$project->misc_cost}}">
                  </div>

                  <div class="form-group">
                    <label for="media_category_id">Media Category</label>
                    <select class="form-control" id="media_category_id" name="media_category_id" value="{{$project->media_category_id}}">
                      @foreach($media_categories as $media_category)
                          <option value="{!! $media_category->id !!}">{!! $media_category->name !!}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="channel_id">Channel</label>
                    <select class="form-control" id="channel_id" name="channel_id" value="{{$project->channel_id}}">
                      @foreach($channels as $channel)
                          @if($channel->id == $project->channel_id)
                            <option selected value="{!! $channel->id !!}">{!! $channel->name !!}</option>
                          @else
                            <option value="{!! $channel->id !!}">{!! $channel->name !!}</option>
                          @endif
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="objective_id">Objective</label>
                    <select class="form-control" id="objective_id" name="objective_id" value="{{$project->objective_id}}">
                      @foreach($objectives as $objective)
                          @if($objective->id == $project->objective_id)
                            <option selected value="{!! $objective->id !!}">{!! $objective->name !!}</option>
                          @else
                            <option value="{!! $objective->id !!}">{!! $objective->name !!}</option>
                          @endif
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="ad_format_id">Ad Format</label>
                    <select class="form-control" id="ad_format_id" name="ad_format_id" value="{{$project->ad_format_id}}">
                      @foreach($ad_formats as $ad_format)
                          @if($ad_format->id == $project->ad_format_id)
                            <option selected value="{!! $ad_format->id !!}">{!! $ad_format->name !!}</option>
                          @else
                            <option value="{!! $ad_format->id !!}">{!! $ad_format->name !!}</option>
                          @endif
                      @endforeach
                    </select>
                  </div>


                  <div class="form-group">

                    <div class="row">
                      <label for="select-from" class="col-md-6">KPI's Availible</label>
                      <label for="select-to" class="col-md-6">KPI's Selected</label>
                    </div>

                    <div class='row'>

                      <div class="col-md-5">

                        <select name="selectfrom" id="select-from" multiple size="5">
                          @foreach($kpis as $kpi)
                              <option value="{!! $kpi->id !!}">{!! $kpi->label !!}</option>
                          @endforeach
                        </select>

                      </div>

                      <div class="col-md-1 text-center">
                        <a><i id="add_kpi_btn" class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></a>
                        <a><i id="remove_kpi_btn" class="fa fa-arrow-left fa-2x" aria-hidden="true"></i></a>
                      </div>

                      <div class="col-md-5">

                        <select name="selectto" id="select-to" multiple size="5">
                          @foreach($selected_kpis as $selected_kpi)
                              <option value="{!! $selected_kpi->id !!}">{!! $selected_kpi->label !!}</option>
                          @endforeach
                        </select>

                      </div>

                    </div>

                  </div>
                  <input type="hidden" name="campaign_id" value="{{$project->campaign_id}}">
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->
        </div><!-- end row -->
      </section>
    </div>
    <!-- /.content-wrapper -->


@endsection
