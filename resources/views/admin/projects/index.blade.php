@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->

        <div class="row">
          <h1 class="text-center">{{$client->name}} - {{$campaign->name}}</h1>
        </div>

        <div class="row">
          <div class="col-md-8 col-md-offset-2 goals">
            <h2>Goals</h2>
            @foreach($goals as $goal)
              <h3>{{ $goal->label }} : {{ $goal->value }}</h3>
            @endforeach
          </div>
        </div>

        <div class="row project_btn_row">
          <div class="col-md-1 col-md-offset-1">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
              New Project
            </button>
          </div>

          <div class="col-md-1 col-md-offset-8">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary   btn-lg" data-toggle="modal" data-target="#goalsModal">
              New Goal
            </button>
          </div>
        </div>

        <div class="row"><!-- start row -->

          <!-- right column -->
          <div class="col-md-10 col-md-offset-1">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Projects Table</h3>
                @if ($projects->isEmpty())
                    <p> There are no campaign.</p>
                @else
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped" data-toggle="table" data-show-toggle="true" data-show-columns="true">
                  <thead>
                    <tr id="headerRow">
                      <th data-field="notes" >Notes</th>
                      <th data-field="name" data-sortable="true">Name</th>
                      <th data-field="status" data-sortable="true">Status</th>
                      <th data-field="start" data-sortable="true">Start Date</th>
                      <th data-field="end" data-sortable="true">End Date</th>
                      <th data-field="budget" data-sortable="true">Budget</th>
                      <th data-field="channel" data-sortable="true">Channel</th>
                      <th data-field="objective" data-sortable="true">Objective</th>
                      <th data-field="format" data-sortable="true">Ad Format</th>
                      <th data-field="report" >Report</th>
                      <th data-field="edit" >Edit</th>
                      <th data-field="delete" >Delete</th>
                    </tr>
                  </thead>
                  <tbody id="tbody">
                  @foreach($projects as $index => $project)
                      <tr data-index="{{ $index }}">
                          <td><span data-toggle="modal" data-target="#modal-{{$project->id}}" class="glyphicon glyphicon-comment"></span></td>
                          <td data-name="name">{!! $project->name !!}</td>
                          <td data-name="status">{!! $project->status !!}</td>
                          <td data-name="start">{!! $project->start_date !!}</td>
                          <td data-name="end">{!! $project->end_date !!}</td>
                          <td data-name="budget">{!! $project->budget !!}</td>
                          <td data-name="channel">{!! $project->channel->label !!}</td>
                          <td data-name="objective">{!! $project->objective->label !!}</td>
                          <td data-name="format">{!! $project->ad_format->label !!}</td>
                          <td>
                            <a class="btn btn-primary" href="<?php echo LARAVEL_URL; ?>/admin/reports/snapshot/{!! $project->id !!}">Report</a>
                          </td>
                          <td>
                            <a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/admin/projects/{!! $project->id !!}/edit">Edit</a>
                          </td>
                          <td>
                          <form class="delete" method="post" action="<?php echo LARAVEL_URL; ?>/admin/projects/{!! $project->id !!}/delete" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                              <div class="form-group">
                                  <div>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                              </div>
                          </form>
                          </td>
                      </tr>

                      <div class="modal fade" id="modal-{{$project->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">{{$project->name}} Notes</h4>
                            </div>
                            <div class="modal-body">
                              @foreach($project->notes as $note)
                                <p>{{$note->body}}</p>
                                <hr />
                              @endforeach
                              <form role="form" method="POST" action="<?php echo LARAVEL_URL; ?>/admin/projects/{!! $project->id !!}/notes">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <input type="hidden" name="project_id" value="{{$project->id}}">
                                <div class="box-body">
                                  <div class="form-group">
                                    <label for="note_body">New Note</label>
                                    <textarea class="form-control" rows="3" id="note_body" name="note_body"></textarea>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Post</button>
                              </form>
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                      </div>



                  @endforeach

                </tbody>
              </table>
              @endif
              </div>
              <!-- /.box-body -->
            </div>

          </div><!--/.col (right) -->
        </div><!-- end row -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create New Project</h4>
              </div>
              <div class="modal-body">
                
                <form role="form" id="create_project" method="POST" action="">
                <!-- error messages -->
                   <p class="alert alert-danger hidden"></p>
                  <!-- status messages -->
                      <div class="alert alert-success hidden"></div>
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body" id="form_inputs_wrap">
                  <div class="form-group">
                    <label for="user_id">Project Creator</label>
                    <input class="form-control" readonly type="text" id="user_id" name="user_id" value="{!! $user_name !!}" />
                  </div>
                  <div class="form-group">
                    <label for="project_name">Project Name</label>
                    <input type="text" class="form-control" id="project_name" name="project_name">
                  </div>
                  <div class="form-group">
                    <label for="project_start_date">Project Start Date</label>
                    <input type="text" class="form-control" id="project_start_date" name="project_start_date">
                  </div>
                  <div class="form-group">
                    <label for="project_end_date">Project End Date</label>
                    <input type="text" class="form-control" id="project_end_date" name="project_end_date">
                  </div>
                  <div class="form-group">
                    <label for="project_budget">Project Budget</label>
                    <input type="text" class="form-control" id="project_budget" name="project_budget">
                  </div>
                  <div class="form-group">
                    <label for="project_labor_cost">Project Labor Cost</label>
                    <input type="text" class="form-control" id="project_labor_cost" name="project_labor_cost">
                  </div>
                  <div class="form-group">
                    <label for="project_misc_cost">Project Misc Cost</label>
                    <input type="text" class="form-control" id="project_misc_cost" name="project_misc_cost">
                  </div>

                  <div class="form-group">
                    <label for="media_category_id">Media Category</label>
                    <select class="form-control" id="media_category_id" name="media_category_id">
                      @foreach($media_categories as $media_category)
                          <option value="{!! $media_category->id !!}">{!! $media_category->name !!}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="channel_id">Channel</label>
                    <select class="form-control" id="channel_id" name="channel_id">
                      @foreach($channels as $channel)
                          <option value="{!! $channel->id !!}">{!! $channel->name !!}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="objective_id">Objective</label>
                    <select class="form-control" id="objective_id" name="objective_id">
                      @foreach($objectives as $objective)
                          <option value="{!! $objective->id !!}">{!! $objective->name !!}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="ad_format_id">Ad Format</label>
                    <select class="form-control" id="ad_format_id" name="ad_format_id">
                      @foreach($ad_formats as $ad_format)
                          <option value="{!! $ad_format->id !!}">{!! $ad_format->name !!}</option>
                      @endforeach
                    </select>
                  </div>

                  <input type="hidden" value="{{ $campaign->id }}" name="campaign_id"></input>
                  <div class="form-group">

                    <div class="row">
                      <label for="select-from" class="col-md-6">KPI's Availible</label>
                      <label for="select-to" class="col-md-6">KPI's Selected</label>
                    </div>

                    <div class='row'>

                      <div class="col-md-5">

                        <select name="selectfrom" id="select-from" multiple size="5">
                          @foreach($kpis as $kpi)
                              <option value="{!! $kpi->id !!}">{!! $kpi->label !!}</option>
                          @endforeach
                        </select>

                      </div>

                      <div class="col-md-1 text-center">
                        <a><i id="add_kpi_btn" class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></a>
                        <a><i id="remove_kpi_btn" class="fa fa-arrow-left fa-2x" aria-hidden="true"></i></a>
                      </div>

                      <div class="col-md-5">

                        <select name="selectto" id="select-to" multiple size="5">

                        </select>

                      </div>

                    </div>

                  </div>

                </div>
                <!-- /.box-body -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Create Project</button>
              </div>
              </form>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal fade" id="goalsModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create New Campaign Goal</h4>
              </div>
              <div class="modal-body">
                <form role="form" method="POST" action=" {{Request::url()}}/goals ">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                  <div class="form-group">
                    <label for="client_id">KPI</label>
                    <select class="form-control" id="kpi_id" name="kpi_id">
                      @foreach($kpis as $kpi)
                          <option value="{!! $kpi->id !!}">{!! $kpi->label !!}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="value">Value</label>
                    <input type="text" name="value" />
                  </div>
                  <input type="hidden" name="campaign_id" value="{{ $campaign->id }}" />
              </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
        </div>

      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
