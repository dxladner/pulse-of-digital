<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>POD | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jQuery UI CSS -->
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <!-- jvectormap -->
  {{-- <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css"> --}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{!! asset('css/AdminLTE.min.css') !!}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{!! asset('css/skins/_all-skins.min.css') !!}">

  <link rel="stylesheet" href="{!! asset('css/sweetalert.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/custom.css') !!}">
  <link rel="stylesheet" href="{!! asset('css/slick.grid.css') !!}">

  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<header class="main-header">
  @php
    $user = auth()->user();
  @endphp
    <!-- Logo -->
    <a href="<?php echo LARAVEL_URL; ?>/admin" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>POD</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>POD</b>Admin</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      {{-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a> --}}

      <!-- Navbar Left Menu -->
      <div class="navbar-custom-left-menu">
          <ul class="nav navbar-nav">
            <!-- CLIENTS DROPDOWN MENU -->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-users fa-lg"></i>
                  {{-- <span class="label label-success">USERS</span> --}}
                </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/clients"><i class="fa fa-user"></i> Create Client</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/campaigns"><i class="fa fa-rocket"></i>Campaigns</a>
                    </li>
                  </ul>
              </li>
              <!-- CAMPAIGN OPTIONS MENU -->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-rocket fa-lg"></i>
                  {{-- <span class="label label-success">USERS</span> --}}
                </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/objectives"><i class="fa fa-object-group"></i> Create Objectives</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/mediacategories"><i class="fa fa-film"></i> Create Media Category</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/channels"><i class="fa fa-video-camera"></i> Create Channels</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/adformats"><i class="fa fa-picture-o"></i> Create Ad Format</a>
                    </li>
                  </ul>
              </li>
              <!-- KPIs DROPDOWN MENU -->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-area-chart fa-lg"></i>
                  {{-- <span class="label label-success">USERS</span> --}}
                </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/kpis"><i class="fa fa-line-chart"></i> Create KPIs</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/kpigroups"><i class="fa fa-bar-chart"></i> Create KPI Group</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/reports"><i class="fa fa-rocket"></i>Reports</a>
                    </li>
                  </ul>
              </li>
            @if( $user->hasRole('admin'))
              <!-- USERS DROPDOWN MENU -->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user-circle fa-lg"></i>
                  {{-- <span class="label label-success">USERS</span> --}}
                </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/users"><i class="fa fa-user-circle-o"></i> Create User</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/users/matrix"><i class="fa fa-user-circle-o"></i> User Matrix</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/roles"><i class="fa fa-shield"></i> Create Role</a>
                    </li>
                  </ul>
                </li>
            @endif

            @if( $user->hasRole('developer'))
              <!-- USERS DROPDOWN MENU -->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user-circle fa-lg"></i>
                  <span class="label label-success">DEVELOPERS</span>
                </a>
                  <ul class="dropdown-menu">
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/users"><i class="fa fa-user-circle-o"></i> Create User</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/users/matrix"><i class="fa fa-user-circle-o"></i> User Matrix</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/permissions"><i class="fa fa-lock"></i> Create Permission</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/roles"><i class="fa fa-shield"></i> Create Role</a>
                    </li>
                    <li>
                      <a href="<?php echo LARAVEL_URL; ?>/admin/roles/matrix"><i class="fa fa-user-circle-o"></i> Role Matrix</a>
                    </li>
                  </ul>
                </li>
            @endif
          </ul>
      </div>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo LARAVEL_URL; ?>/img/user1-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo LARAVEL_URL; ?>/img/user1-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo LARAVEL_URL; ?>/img/user1-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo LARAVEL_URL; ?>/img/user1-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="<?php echo LARAVEL_URL; ?>/img/user1-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo LARAVEL_URL; ?>/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">
                @if (Auth::check())
                  <span class="username">{{ Auth::user()->name }}</span>
                @else
                  <p>Please login!</p>
                @endif
              </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo LARAVEL_URL; ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                <p>
                  @if (Auth::check())
                    <span class="username">{{ Auth::user()->name }}</span>

                    @if(is_null($roleNameArray))
                      <p>
                        You have no roles.
                      </p>
                    @else
                      @foreach($roleNameArray as $name)
                        <small>role: {{ $name->name }}</small>
                      @endforeach
                    @endif
                @else
                  <p>Please login!</p>
                @endif
                </p>
              </li>

              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">
                    <i class="fa fa-btn fa-user"></i> Profile
                  </a>
                </div>
                <div class="pull-right">
                  <a href="http://localhost/lararoles/public/logout" class="btn btn-default btn-flat">
                    <i class="fa fa-btn fa-sign-out"></i> Sign out
                  </a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">



    @yield('content')

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
      </div>
      <strong>Copyright &copy; 2017 <a href="http://ivie..com">Ivie</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">

      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">



          <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->

        <!-- Settings tab content -->

        <!-- /.tab-pane -->
      </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->
<!-- sweetalert -->
<script src="{!! asset('js/sweetalert.min.js') !!}"></script>

@include('Alerts::show')

<!-- jQuery 2.2.3 -->
<script src="{!! asset('js/jquery-2.2.3.min.js') !!}"></script>
<script src="{!! asset('js/jquery.event.drag-2.2.js') !!}"></script>
<!-- jQuery UI 1.12.1 -->
<script src="{!! asset('js/jquery-ui-1.12.1.js') !!}"></script>
<!-- Vue JS 1.0.28 -->
<script src="{!! asset('js/vue.js') !!}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{!! asset('js/bootstrap.min.js') !!}"></script>
<!-- FastClick -->
<script src="{!! asset('js/plugins/fastclick/fastclick.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset('js/app.min.js') !!}"></script>
<!-- Sparkline -->
<script src="{!! asset('js/plugins/sparkline/jquery.sparkline.min.js') !!}"></script>
<!-- jvectormap -->
<script src="{!! asset('js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') !!}"></script>
<script src="{!! asset('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{!! asset('js/plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{!! asset('js/plugins/chartjs/Chart.min.js') !!}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
{{-- <script src="{!! asset('js/pages/dashboard2.js') !!}"></script> --}}
<!-- AdminLTE for demo purposes -->
<script src="{!! asset('js/demo.js') !!}"></script>
<script src="{!! asset('js/custom.js') !!}"></script>

<script src="{!! asset('js/slick.core.js') !!}"></script>
<script src="{!! asset('js/slick.grid.js') !!}"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>

<!-- Latest compiled and minified Locales -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/locale/bootstrap-table-zh-CN.min.js"></script>

</body>
</html>
