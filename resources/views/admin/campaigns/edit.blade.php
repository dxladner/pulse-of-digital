@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit a Campaign</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="">
                <!-- error messages -->
                  @foreach ($errors->all() as $error)
                      <p class="alert alert-danger">{{ $error }}</p>
                  @endforeach
                  <!-- status messages -->
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="campaign_name">Campaign Name</label>
                    <input type="text" class="form-control" id="campaign_name" name="campaign_name" value="{!! $campaign->name !!}">
                  </div>
                  <div class="form-group">
                    <label for="campaign_status">Campaign Status</label>
                    <select class="form-control" id="campaign_status" name="campaign_status">
                      <option value="{!! $campaign->status !!}">{!! $campaign->status !!}</option>
                      <option value="active">Active</option>
                      <option value="suspend">Suspend</option>
                      <option value="archive">Archive</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="campaign_description">Campaign Description</label>
                    <input type="text" class="form-control" id="campaign_description" name="campaign_description" value="{!! $campaign->description !!}">
                  </div>
                  <div class="form-group">
                    <label for="campaign_start_date">Campaign Start Date</label>
                    <input type="text" class="form-control" id="campaign_start_date" name="campaign_start_date" value="{!! $campaign->start_date !!}">
                  </div>
                  <div class="form-group">
                    <label for="campaign_end_date">Campaign End Date</label>
                    <input type="text" class="form-control" id="campaign_end_date" name="campaign_end_date" value="{!! $campaign->end_date !!}">
                  </div>
                  <div class="form-group">
                    <label for="campaign_budget">Campaign Budget</label>
                    <input type="text" class="form-control" id="campaign_budget" name="campaign_budget" value="{!! $campaign->budget !!}">
                  </div>
                  <div class="form-group">
                    <label for="client_id">Client</label>

                    <select class="form-control" id="client_id" name="client_id">
                      <option value="{!! $client_id !!}">{!! $client_name !!}</option>
                      @foreach($clients as $client)
                          <option value="{!! $client->id !!}">{!! $client->name !!}</option>
                      @endforeach

                    </select>

                  </div>

                  <input type="hidden" id="user_id" name="user_id" value="{!! $user_id !!}"  />
                  <input type="hidden" id="role_id" name="role_id" value="{!! $role_id !!}"  />
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->
        </div><!-- end row -->
      </section>
    </div>
    <!-- /.content-wrapper -->


@endsection
