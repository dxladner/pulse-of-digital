@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->

        <div class="row">
          <h1 class="text-center">Campaigns</h1>
        </div>

        <div class="row project_btn_row">
          <div class="col-md-1 col-md-offset-1">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#myModal">
              New Campaign
            </button>
          </div>
        </div>

        <div class="row"><!-- start row -->

          <!-- right column -->
          <div class="col-md-10 col-md-offset-1">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Campaign Table</h3>
                @if ($campaigns->isEmpty())
                    <p> There are no campaign.</p>
                @else
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped" data-toggle="table" data-show-toggle="true" data-show-columns="true">
                  <thead>
                      <tr>
                        <th data-field="notes">Notes</th>
                        <th data-field="name" data-sortable="true">Name</th>
                        <th data-field="status" data-sortable="true">Status</th>
                        <th data-field="start" data-sortable="true">Start Date</th>
                        <th data-field="end" data-sortable="true">End Date</th>
                        <th data-field="budget" data-sortable="true">Budget</th>
                        <th data-field="edit">Edit</th>
                        <th data-field="delete">Delete</th>
                      </tr>
                    </thead>
                  <tbody>
                  @foreach($campaigns as $index => $campaign)
                      <tr class="campaign" id="{{ $campaign->id }}" data-index="{{ $index }}">
                        <a href="<?php echo LARAVEL_URL; ?>/admin/campaigns/{!! $campaign->id !!}">
                          <td><span data-toggle="modal" data-target="#modal-{{$campaign->id}}" class="glyphicon glyphicon-comment"></span></td>
                          <td data-name="name"><a href="<?php echo LARAVEL_URL; ?>/admin/campaigns/{!! $campaign->id !!}">{!! $campaign->name !!}</a></td>
                          <td data-name="status">{!! $campaign->status !!}</td>
                          <td data-name="start">{!! $campaign->start_date !!}</td>
                          <td data-name="end">{!! $campaign->end_date !!}</td>
                          <td data-name="budget">{!! $campaign->budget !!}</td>
                          <td>
                            <a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/admin/campaigns/{!! $campaign->id !!}/edit">Edit</a>
                          </td>
                          <td>
                          <form class="delete" method="post" action="<?php echo LARAVEL_URL; ?>/admin/campaigns/{!! $campaign->id !!}/delete" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                              <div class="form-group">
                                  <div>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                              </div>
                          </form>
                          </td>
                        </a>
                      </tr>

                      <div class="modal fade" id="modal-{{$campaign->id}}" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title">{{$campaign->name}} Notes</h4>
                            </div>
                            <div class="modal-body">
                              @foreach($campaign->notes as $note)
                                <p>{{$note->body}}</p>
                                <hr />
                              @endforeach
                              <form role="form" method="POST" action="<?php echo LARAVEL_URL; ?>/admin/campaigns/{!! $campaign->id !!}/notes">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <input type="hidden" name="campaign_id" value="{{$campaign->id}}">
                                <div class="box-body">
                                  <div class="form-group">
                                    <label for="note_body">New Note</label>
                                    <textarea required minlength="2" class="form-control" rows="3" id="note_body" name="note_body"></textarea>
                                  </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Post</button>
                              </form>
                            </div><!-- /.modal-content -->
                          </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                      </div> 

                  @endforeach
                </tbody>
              </table>
              @endif
              </div>
              <!-- /.box-body -->
            </div>

          </div><!--/.col (right) -->
        </div><!-- end row -->

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Create New Project</h4>
              </div>
              <div class="modal-body">
                <form role="form" method="POST" action="">
                <!-- error messages -->
                  @foreach ($errors->all() as $error)
                      <p class="alert alert-danger">{{ $error }}</p>
                  @endforeach
                  <!-- status messages -->
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="campaign_name">Campaign Name</label>
                    <input type="text" class="form-control" id="campaign_name" name="campaign_name">
                  </div>
                  <div class="form-group">
                    <label for="campaign_description">Campaign Description</label>
                    <input type="text" class="form-control" id="campaign_description" name="campaign_description">
                  </div>
                  <div class="form-group">
                    <label for="campaign_start_date">Campaign Start Date</label>
                    <input type="text" class="form-control" id="campaign_start_date" name="campaign_start_date">
                  </div>
                  <div class="form-group">
                    <label for="campaign_end_date">Campaign End Date</label>
                    <input type="text" class="form-control" id="campaign_end_date" name="campaign_end_date">
                  </div>
                  <div class="form-group">
                    <label for="campaign_budget">Campaign Budget</label>
                    <input type="text" class="form-control" id="campaign_budget" name="campaign_budget">
                  </div>
                  <div class="form-group">
                    <label for="client_id">Client</label>
                    <select class="form-control" id="client_id" name="client_id">
                      @foreach($clients as $client)
                          <option value="{!! $client->id !!}">{!! $client->name !!}</option>
                      @endforeach
                    </select>
                  </div>
                  <input type="hidden" id="user_id" name="user_id" value="{!! $user_id !!}"  />
                  <input type="hidden" id="role_id" name="role_id" value="{!! $role_id !!}"  />
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
