@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Edit an Objective</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" id="edit_objective" action="">
                <!-- error messages -->
                  @foreach ($errors->all() as $error)
                      <p class="alert alert-danger">{{ $error }}</p>
                  @endforeach
                  <!-- status messages -->
                  @if (session('status'))
                      <div class="alert alert-success">
                          {{ session('status') }}
                      </div>
                  @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="objective_name">Objective Name</label>
                    <input type="text" class="form-control" id="objective_name" name="objective_name" value="{!! $objective->name !!}">
                  </div>
                  <div class="form-group">
                    <label for="objective_label">Objective Label</label>
                    <input type="text" class="form-control" id="objective_label" name="objective_label" value="{!! $objective->label !!}">
                  </div>

                  <div class="form-group">

                    <div class="row">
                      <label for="select-from" class="col-md-6">KPI's Availible</label>
                      <label for="select-to" class="col-md-6">KPI's Selected</label>
                    </div>

                    <div class='row'>

                      <div class="col-md-5">

                        <select style="width:100%;" name="selectfrom" id="select-from" multiple size="5">
                          @foreach($kpis as $kpi)
                              <option value="{!! $kpi->id !!}">{!! $kpi->label !!}</option>
                          @endforeach
                        </select>

                      </div>

                      <div class="col-md-1 text-center">
                        <a style="display:block;"><i id="add_kpi_btn" style="margin-bottom: 5%;" class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></a>
                        <a><i id="remove_kpi_btn" class="fa fa-arrow-left fa-2x" aria-hidden="true"></i></a>
                      </div>

                      <div class="col-md-5">

                        <select style="width:100%;" name="selectto" id="select-to" multiple size="5">
                          @foreach($selected_kpis as $selected_kpi)
                              <option value="{!! $selected_kpi->id !!}">{!! $selected_kpi->label !!}</option>
                          @endforeach
                        </select>

                      </div>

                    </div>

                  </div>


                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->
        </div><!-- end row -->
      </section>
    </div>
    <!-- /.content-wrapper -->


@endsection
