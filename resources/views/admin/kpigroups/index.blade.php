@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Create a KPI Group</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" id="create_kpi_group" method="POST" action="">
                <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  
                  <div class="form-group">
                    <label for="channel_id">Channel</label>
                    <select class="form-control" id="channel_id" name="channel_id">
                      @foreach($channels as $channel)
                          <option value="{!! $channel->id !!}">{!! $channel->name !!}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="ad_format_id">Ad Format</label>
                    <select class="form-control" id="ad_format_id" name="ad_format_id">
                      @foreach($ad_formats as $ad_format)
                          <option value="{!! $ad_format->id !!}">{!! $ad_format->name !!}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">

                    <div class="row">
                      <label for="select-from" class="col-md-6">KPI's Availible</label>
                      <label for="select-to" class="col-md-6">KPI's Selected</label>
                    </div>

                    <div class='row'>

                      <div class="col-md-5">

                        <select style="width:100%;" name="selectfrom" id="select-from" multiple size="5">
                          @foreach($kpis as $kpi)
                              <option value="{!! $kpi->id !!}">{!! $kpi->label !!}</option>
                          @endforeach
                        </select>

                      </div>

                      <div class="col-md-1 text-center">
                        <a><i id="add_kpi_btn" style="margin-bottom: 5%;" class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></a>
                        <a><i id="remove_kpi_btn" class="fa fa-arrow-left fa-2x" aria-hidden="true"></i></a>
                      </div>

                      <div class="col-md-5">

                        <select style="width:100%;" name="selectto" id="select-to" multiple size="5">

                        </select>

                      </div>

                    </div>

                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->

        </div><!-- end row -->

      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
