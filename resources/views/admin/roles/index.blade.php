@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Create a Role</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="">
                <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="role_name">Role Name</label>
                    <input type="text" class="form-control" id="role_name" name="role_name">
                  </div>
                  <div class="form-group">
                    <label for="role_label">Role Label</label>
                    <input type="text" class="form-control" id="role_label" name="role_label">
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->
        </div><!-- end row -->

        <div class="row">
          <!-- right column -->
          <div class="col-md-12">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Roles Table</h3>
                @if ($roles->isEmpty())
                    <p> There are no roles.</p>
                @else
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped">
                  <tbody>

                    <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    <th>Add Permissions</th>
                  </tr>
                  @foreach($roles as $role)
                      <tr>
                          <td>{!! $role->id !!}</td>
                          <td>{!! $role->name !!}</td>
                          <td>{!! $role->label !!}</td>
                          <td>
                            <a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/admin/roles/{!! $role->id !!}/edit">Edit</a>
                          </td>
                          <td>
                          <form class="delete" method="post" action="<?php echo LARAVEL_URL; ?>/admin/roles/{!! $role->id !!}/delete" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                              <div class="form-group">
                                  <div>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                              </div>
                          </form>
                          </td>
                          <td>
                            <a class="btn btn-success" href="<?php echo LARAVEL_URL; ?>/admin/roles/{!! $role->id !!}/add">Add</a>
                          </td>
                      </tr>
                  @endforeach

                </tbody>
              </table>
              @endif
              </div>
              <!-- /.box-body -->
            </div>

          </div>
        </div>

      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
