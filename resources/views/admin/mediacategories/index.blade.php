@extends('admin.layouts.master')

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <section class="content"><!-- section content -->
        <div class="row"><!-- start row -->
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Create a Media Category</h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form role="form" method="POST" action="">
                <!-- error messages -->
	                @foreach ($errors->all() as $error)
	                    <p class="alert alert-danger">{{ $error }}</p>
	                @endforeach
	                <!-- status messages -->
	                @if (session('status'))
	                    <div class="alert alert-success">
	                        {{ session('status') }}
	                    </div>
	                @endif
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="box-body">
                  <div class="form-group">
                    <label for="media_category_name">Media Category Name</label>
                    <input type="text" class="form-control" id="media_category_name" name="media_category_name">
                  </div>
                  <div class="form-group">
                    <label for="media_category_label">Media Category Label</label>
                    <input type="text" class="form-control" id="media_category_label" name="media_category_label">
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.box -->
          </div>
          <!--/.col (left) -->

          <!-- right column -->
          <div class="col-md-6">

            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Media Categories Table</h3>
                @if ($media_categories->isEmpty())
                    <p> There are no objectives.</p>
                @else
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <table class="table table-striped">
                  <tbody>

                    <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Label</th>
                    <th>Edit</th>
                    <th>Delete</th>
                  </tr>
                  @foreach($media_categories as $media_category)
                      <tr>
                          <td>{!! $media_category->id !!}</td>
                          <td>{!! $media_category->name !!}</td>
                          <td>{!! $media_category->label !!}</td>
                          <td>
                            <a class="btn btn-warning" href="<?php echo LARAVEL_URL; ?>/admin/mediacategories/{!! $media_category->id !!}/edit">Edit</a>
                          </td>
                          <td>
                          <form class="delete" method="post" action="<?php echo LARAVEL_URL; ?>/admin/mediacategories/{!! $media_category->id !!}/delete" >
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                              <div class="form-group">
                                  <div>
                                      <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                              </div>
                          </form>
                          </td>
                      </tr>
                  @endforeach

                </tbody>
              </table>
              @endif
              </div>
              <!-- /.box-body -->
            </div>

          </div><!--/.col (right) -->
        </div><!-- end row -->

      </section>
    </div>
    <!-- /.content-wrapper -->

@endsection
