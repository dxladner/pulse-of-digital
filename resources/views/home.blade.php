@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                  @if (Auth::check())
                    <p class="username">
                      {{ Auth::user()->name }}'s Dashboard
                    </p>
                  @else
                    <p>Please login!</p>
                  @endif
                </div>

                <div class="panel-body">
                    {{ Auth::user()->name }}, you are logged in!
                </div>

                @can('create_user')
                  <a href="#">Create A User</a>
                @endcan
                @can('create_project')
                  <a href="#">Create A Project</a>
                @endcan
                @can('create_channel')
                  <a href="#">Create A Channel</a>
                @endcan
            </div>
        </div>
    </div>
  
</div>

@endsection
