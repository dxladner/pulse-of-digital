@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome to Pulse of Digital</div>

                <div class="panel-body">
                    Pulse of Digital
                </div>

                @can('create_user')
                  <a href="#">Create A User</a>
                @endcan
                @can('create_channel')
                  <a href="#">Create A Channel</a>
                @endcan
            </div>
        </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <div id="storiesGrid" style="width:600px;height:500px;"></div>
      </div>
    </div>

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
          Add User
        </button>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body">

                  <div id="app">
                      <h1>@{{ message }}</h1>


                      <ul class="list-group">
                        <user v-for="user in users" :user="user">
                        </user>
                      </ul>
                      <hr />
                      <form id="addUser" name="addUser" action="http://localhost/lararoles/public/api/users" method="post" role="form">
                          <input type="hidden" name="_token" value="{!! csrf_token() !!}">
      									<div class="form-group">
      										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
      									</div>
      									<div class="form-group">
      										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
      									</div>
      									<div class="form-group">
      										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
      									</div>
      									<div class="form-group">
      										<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">
      									</div>
      									<div class="form-group">
      										<div class="row">
      											<div class="col-sm-6 col-sm-offset-3">
      												<button @click="addUser(user)" class="btn btn-primary">
                                Add User
                              </button>
      											</div>
      										</div>
      									</div>
      								</form>
                      <hr />
                      @{{ $data | json }}
                  </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
            <script type="text/template" id="template-user-raw">

                <li class="list-group-item">
                  @{{ user.id }} ||  @{{ user.name }} || @{{user.email}}
                </li>

              <hr />

            </script>


        </div>
</div>
@endsection
