<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiObjectiveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('kpi_objective', function (Blueprint $table) {
          $table->integer('kpi_id')->unsigned();
          $table->integer('objective_id')->unsigned();
          $table->foreign('kpi_id')->references('id')->on('kpis');
          $table->foreign('objective_id')->references('id')->on('objectives');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('kpi_objective', function (Blueprint $table){
        $table->dropForeign(['kpi_id']);
        $table->dropForeign(['objective_id']);
      });
    }
}
