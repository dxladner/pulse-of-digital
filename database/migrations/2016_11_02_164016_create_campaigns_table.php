<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('campaigns', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('status');
          $table->text('description');
          $table->date('start_date');
          $table->date('end_date');
          $table->integer('budget');

          $table->integer('client_id')->unsigned();
          $table->foreign('client_id')->references('id')->on('clients');
          $table->integer('user_id')->unsigned();
          $table->foreign('user_id')->references('id')->on('users');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaigns');
    }
}
