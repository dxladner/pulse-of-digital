<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpiProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('kpi_project', function (Blueprint $table) {
          $table->integer('project_id')->unsigned();
          $table->integer('kpi_id')->unsigned();
          $table->foreign('project_id')->references('id')->on('projects');
          $table->foreign('kpi_id')->references('id')->on('kpis');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kpi_project', function (Blueprint $table){
          $table->dropForeign(['project_id']);
          $table->dropForeign(['kpi_id']);
        });
    }
}
