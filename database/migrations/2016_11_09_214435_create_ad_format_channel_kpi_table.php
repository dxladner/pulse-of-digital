<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdFormatChannelKpiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ad_format_channel_kpi', function (Blueprint $table) {
          $table->integer('ad_format_id')->unsigned();
          $table->integer('channel_id')->unsigned();
          $table->integer('kpi_id')->unsigned();

          $table->foreign('ad_format_id')->references('id')->on('ad_formats');
          $table->foreign('channel_id')->references('id')->on('channels');
          $table->foreign('kpi_id')->references('id')->on('kpis');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('ad_format_channel_kpi', function (Blueprint $table){
        $table->dropForeign(['ad_format_id']);
        $table->dropForeign(['channel_id']);
        $table->dropForeign(['kpi_id']);
      });
    }
}
