<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('campaign_role', function (Blueprint $table) {
          $table->integer('campaign_id')->unsigned();
          $table->integer('role_id')->unsigned();
          $table->foreign('campaign_id')->references('id')->on('campaigns');
          $table->foreign('role_id')->references('id')->on('roles');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('campaign_role');
    }
}
