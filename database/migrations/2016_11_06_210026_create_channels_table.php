<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('channels', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->string('label');
          $table->integer('media_category_id')->unsigned();
          $table->foreign('media_category_id')->references('id')->on('media_categories');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('channels', function (Blueprint $table){
        $table->dropForeign(['media_category_id']);
      });
    }
}
