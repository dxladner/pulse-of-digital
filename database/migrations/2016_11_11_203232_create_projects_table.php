<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('projects', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->date('start_date');
          $table->date('end_date');
          $table->integer('budget');
          $table->integer('labor_cost');
          $table->string('status');
          $table->integer('misc_cost');

          $table->integer('channel_id')->unsigned();
          $table->integer('media_category_id')->unsigned();
          $table->integer('campaign_id')->unsigned();
          $table->integer('ad_format_id')->unsigned();
          $table->integer('objective_id')->unsigned();
          $table->integer('user_id')->unsigned();

          $table->foreign('channel_id')->references('id')->on('channels');
          $table->foreign('media_category_id')->references('id')->on('media_categories');
          $table->foreign('campaign_id')->references('id')->on('campaigns');
          $table->foreign('ad_format_id')->references('id')->on('ad_formats');
          $table->foreign('objective_id')->references('id')->on('objectives');
          $table->foreign('user_id')->references('id')->on('users');

          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('projects', function (Blueprint $table){
        $table->dropForeign(['channel_id']);
        $table->dropForeign(['media_category_id']);
        $table->dropForeign(['campaign_id']);
        $table->dropForeign(['ad_format_id']);
        $table->dropForeign(['objective_id']);
        $table->dropForeign(['user_id']);
      });
    }
}
