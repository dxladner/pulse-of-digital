Vue.http.headers.common['X-CSRF-TOKEN'] = $("#token").attr("value");
Vue.component('user', {
  template: '#template-user-raw',
  props: ['user'],
  methods: {
    addUser: function(user){
        $.ajax({
          url: '/api/users/'+user.id,
          type: 'PATCH',
          data: user,
        });
    }
  },
});

var vm = new Vue({

    el: '#app',

    data: {
        users: []
    },

    ready: function(){
      $.get('http://localhost/lararoles/public/api/users', function(data){

        vm.users = data;
      })
    }
})

// var data = {
//   message: 'Let\'s hear some stories!'
// };
//
// new Vue({
//   el: '#app',
//   data: data
// })
