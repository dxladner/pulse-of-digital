$( document ).ready(function() {
    $( "#project_start_date" ).datepicker();
    $( "#project_end_date" ).datepicker();
    $( "#campaign_start_date" ).datepicker();
    $( "#campaign_end_date" ).datepicker();
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
    })

    var edit_project_pattern = "/pulse-of-digital/public/admin/projects/[0-9]+\/edit";
    var view_report_pattern = "/pulse-of-digital/public/admin/reports/[0-9]+";

    if(!window.location.pathname.match(edit_project_pattern)) {
      $("#channel_id").change();
    }

    if(window.location.pathname.match(view_report_pattern)) {
      var report_id = $('#report_id').val();
      retrieveGridSnaps(report_id);
    }

});

function retrieveGridSnaps (reportId) {

  data = 'report_id=' + reportId;

  $.ajax({
        type:"GET",
        url:'/pulse-of-digital/public/admin/reports/' + reportId + '/build',
        data: data,
        dataType: 'json',
        success: function(data){

           buildGrid(data);
        }//End Success
    });
}

function buildGrid (data) {
  var rows = [];
  for(i=0; i < data['snapshots'].length; i++) {
      var snap = data['snapshots'][i];
      rows[i] = {
        name: snap.name,
        spend: snap.spend
      };
  };

  var cols = [];
  console.log(rows)
  for(i=0; i<data['snapshots'].length; i++) {
    data['snapshots'][i]['kpis'] = JSON.parse(data['snapshots'][i]['kpis']);
    for(j = 0; j < data['snapshots'][i]['kpis'].length; j++) {
      for(k in data['snapshots'][i]['kpis'][j]) {

        rows[i][k] = data['snapshots'][i]['kpis'][j][k];

        if(cols.includes(k) == false) {
          cols.push(k);
        }

      }
    }
  }

  var grid;
    var columns = [
      { id: "name", name: "Name", field: "name", sortable: true },
      { id: "spend", name: "spend", field: "spend", sortable: true }
    ];

    for(i = 0; i < cols.length; i++) {
      columns.push({ id: cols[i], name: cols[i], field: cols[i], sortable: true });
      for(j = 0; j < rows.length; j++) {
        if(!rows[j].hasOwnProperty(cols[i])){
          rows[j][cols[i]] = '';
        }
      }
    }


    var options = {
      enableCellNavigation: true,
      enableColumnReorder: false,
      multiColumnSort: true
    };

  $(function() {

    grid = new Slick.Grid("#myGrid", rows, columns, options);

    grid.onSort.subscribe(function (e, args) {
      var grid_cols = args.sortCols;

      rows.sort(function (dataRow1, dataRow2) {
        for (var i = 0, l = grid_cols.length; i < l; i++) {
          var field = grid_cols[i].sortCol.field;
          var sign = grid_cols[i].sortAsc ? 1 : -1;
          var value1 = dataRow1[field], value2 = dataRow2[field];
          var result = (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
          if (result != 0) {
            return result;
          }
        }
        return 0;
      });
      grid.invalidate();
      grid.render();
    });
  })
}


/* ADD/REMOVE KPI TO PROJECT */
$("#add_kpi_btn").click(function(){
  $('#select-from option:selected').each( function() {
            $('#select-to').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
        $(this).remove();
    });
});

$('#remove_kpi_btn').click(function(){
    $('#select-to option:selected').each( function() {
        $('#select-from').append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
        $(this).remove();
    });
});

$("#channel_id, #ad_format_id, #objective_id").change(function () {
    var channel_id = $('#channel_id').val();
    var ad_format_id = $('#ad_format_id').val();
    var objective_id = $('#objective_id').val();

    data = 'ad_format_id=' + ad_format_id + '&channel_id=' + channel_id;


    if(objective_id !== undefined) {
       data = 'ad_format_id=' + ad_format_id + '&channel_id=' + channel_id + '&objective_id=' + objective_id;
    }

    $.ajax({

        type:"GET",
        url:'/pulse-of-digital/public/admin/projects/associatekpis',
        data: data,
        dataType: 'json',
        success: function(data){
            $("#select-to > option").each(function() {
                $('#select-from').append(this);
            });

            for(var i = 0; i< data.response.length; i++) {
                $("#select-from > option").each(function() {

                    if(this.value == data.response[i].id) {
                        $('#select-to').append(this);
                        return false;
                    }
                });
            }
        }
    });
});

$('#create_project, #create_kpi_group, #create_objective, #edit_objective, #edit_project').on('submit', function(e) {

    e.preventDefault(e);
  
    var redirect = false;

    if($(this).attr('id') == 'edit_project' || $(this).attr('id') == 'edit_objective') {
      redirect = true;
    }

    var selectbox = document.getElementById('select-to'),
        kpis = [],
        data = $(this).serialize();

    for(i = 0; i< selectbox.options.length; i++) {
        data += '&kpis[' + i +']=' + selectbox.options[i].value;
    }

    $.ajax({

        type:"POST",
        url:'',
        data: data,
        dataType: 'json',
        success: function(data){
          swal({
            title: 'Success!',
            text: data['responseText'],
            timer: 1000,
            showConfirmButton: false,
            type: "success"
          }, function() {
              if(redirect) {
                history.go(-1);
              } else {
                location.reload();
              }
          });
          
        },
        error: function(data){
            swal({
              title: 'Oops!',
              text: 'Something went wrong',
              timer: 1000,
              showConfirmButton: false,
              type: "error"
            },function() {
                location.reload();
          });
        }
    });
});

$('.campaign, .report').click(function(event) {

  if($(event.target).attr('class') == 'btn btn-danger' || $(event.target).attr('class') == 'glyphicon glyphicon-comment') {
    return;  
  }
  console.log('clicked');
  
  var url =  window.location.href + '/' + $(this).attr('id');
  $( location ).attr("href", url);

});

$('.delete').on('submit', function(e) {
    e.preventDefault(e);

    var target_url = $(this).attr('action');
    data = $(this).serialize();

    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this data!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Confirm",
      closeOnConfirm: false
    }, function(){
       $.ajax({

        type:"POST",
        url: target_url,
        data: data,
        dataType: 'json',
        success: function(data){
          swal({
            title: 'Success!',
            text: data['responseText'],
            timer: 1000,
            showConfirmButton: false,
            type: "success"
          }, function() {
                location.reload();
          });
          
        },
        error: function(data){
            swal({
              title: 'Oops!',
              text: 'Something went wrong',
              timer: 1000,
              showConfirmButton: false,
              type: "error"
            },function() {
                location.reload();
            });
          }
        });
    });

});

$("#report_campaign_id").change(function () {

    console.log('changed');
    var campaign_id = $('#report_campaign_id').val();

    data = 'campaign_id=' + campaign_id;

    $.ajax({

        type:"GET",
        url:'/pulse-of-digital/public/admin/reports/campaign/' + campaign_id,
        data: data,
        dataType: 'json',
        success: function(data){
          $('.snapshot_wrap').empty();

          for(i = 0; i < data['response'].length; i++){
            console.log(data['response'][i]['id']);
        
            var html = '<div class="form-group"><input type="hidden" value="' + data['response'][i]['id'] + '" name="snapshot_id[]">' + data['response'][i]['name'] + '</input></div>'
            $('.snapshot_wrap').append(html);
          }
        }
    });

});
