  var grid;

  var columns = [
    {id: "id", name: "ID", field: "id"},
    {id: "plot", name: "Plot", field: "plot"},
    {id: "upvotes", name: "Upvotes", field: "upvotes"},
    {id: "writer", name: "Writer", field: "writer"}
  ];

  var options = {
    enableCellNavigation: true,
    enableColumnReorder: false
  };
  
  $(function () {
      var myData = [];
      $.getJSON('http://localhost/laravel50/public/api/stories', function (data) {
          myData = data;
          grid = new Slick.Grid("#storiesGrid", myData, columns, options);
      });
  });
  // $(function () {
  //   var data = [
  //     {
  //       "id": 1,
  //       "plot": "story of space wars between good and evil",
  //       "upvotes": 12,
  //       "writer": "George Vader",
  //       "created_at": "2016-09-01 10:00:00",
  //       "updated_at": null
  //       },
  //       {
  //       "id": 2,
  //       "plot": "comedy of events at a golf club",
  //       "upvotes": 21,
  //       "writer": "Chevy Murray",
  //       "created_at": null,
  //       "updated_at": null
  //       },
  //       {
  //       "id": 3,
  //       "plot": "murder mystery in the mountains",
  //       "upvotes": 9,
  //       "writer": "John Doe",
  //       "created_at": "2016-09-13 10:00:00",
  //       "updated_at": null
  //       },
  //       {
  //       "id": 4,
  //       "plot": "documentary of the white hats in Syria",
  //       "upvotes": 17,
  //       "writer": "Lillian Jones",
  //       "created_at": "2016-09-15 10:00:00",
  //       "updated_at": null
  //       }
  //   ];
  //
  //   grid = new Slick.Grid("#storiesGrid", data, columns, options);
  // })
