# POD Built using the Laravel PHP Framework

## Official Documentation

### How to use Roles and Permission:

#### To check a role in controller:
```
if ( ( $user->hasRole( 'admin' ) ) || ( $user->hasRole( 'developer' ) ) )
{

}
```

#### To check a permission in a view:
```
@if(Auth::user()->can('edit_user'))

@endif
```
